#$ -S /bin/tcsh
#
# script to perform Dark Matter clumpy simulations
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

set PARAMETER=RRRRR
set RUNDIR=RUUUN
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DAATE

# Geometrical parameters:
set psiZeroDeg=AAAAA
set thetaZeroDegGal=BBBBB
set psiWidthDeg=CCCCC
set thetaWidthDeg=DDDDD

# CLUMPY technical parameters:
set user_rse=FFFFF
set seed=ZHHHH
set alphaIntDeg=EEEEE

# Galactic Halo:
set gGAL_TOT_FLAG_PROFILE=HHHHH	
set gGAL_TOT_SHAPE_PARAMS_0=IIIII
set gGAL_TOT_SHAPE_PARAMS_1=JJJJJ
set gGAL_TOT_SHAPE_PARAMS_2=KKKKK
set gGAL_TOT_RSCALE=LLLLL
set gGAL_RHOSOL=MMMMM
set gGAL_RSOL=NNNNN
set gGAL_RVIR=OOOOO

# Clumps distribution:
set gGAL_DPDV_FLAG_PROFILE=PPPPP
set gGAL_DPDV_SHAPE_PARAMS_0=QQQQQ
set gGAL_DPDV_SHAPE_PARAMS_1=SSSSS
set gGAL_DPDV_SHAPE_PARAMS_2=TTTTT
set gGAL_DPDV_RSCALE=UUUUU
set gGAL_DPDM_SLOPE=VVVVV
set gDM_MMIN_SUBS=WWWWW
set gGAL_SUBS_N_INM1M2=XXXXX

# Clumps inner profile:
set gGAL_CLUMPS_FLAG_PROFILE=YYYYY
set gGAL_CLUMPS_SHAPE_PARAMS_0=ZZZZZ
set gGAL_CLUMPS_SHAPE_PARAMS_1=ZAAAA
set gGAL_CLUMPS_SHAPE_PARAMS_2=ZBBBB
set gGAL_CLUMPS_FLAG_CVIRMVIR=ZCCCC

# Additional clumpy parameters:
set gDM_MMAXFRAC_SUBS=ZDDDD
set gDM_RHOSAT=ZEEEE
set gGAL_SUBS_M1=ZFFFF
set gGAL_SUBS_M2=ZGGGG

# Particle physics parameters:
set gSIMU_IS_ANNIHIL_OR_DECAY=GGGGG

# Halo list:
set gLIST_HALOES=ZIIII
set gLIST_Bool=ZJJJJ

# technical:
set realisationnumber=ZKKKK
set onlyOneRunBool=ZLLLL

# temporary directory
set DDIR=$TMPDIR/Moritz
mkdir -p $DDIR
rm -f $DDIR/*
echo $DDIR

echo
echo " **************************************"
echo " This is a clumpy simulation"
echo " **************************************"
echo 
echo " *** INPUT PARAMETERS: ***"
echo 
echo -n " Psi position of line of sight: " $psiZeroDeg "degrees\n"
echo -n " Theta position of line of sight: " $thetaZeroDeg "degrees\n"
echo -n " Full width of skymap around line of sight in psi direction: " $psiWidthDeg "degrees\n"
echo -n " Full width of skymap around line of sight in theta direction: " $thetaWidthDeg "degrees\n"
echo -n " grid resolution: " $alphaIntDeg "degrees\n"
echo -n " user_rse: " $user_rse "\n"
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE dmClumps.sub.sh ***"
echo 

###############################################################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_oldROOT.tcsh VERITAS
echo

###############################################################################
# explanation of different directories:
# RUNDIR:  parent directory of run, the one set in line (08) of
#		   anisotropySimulation.parameters file, e.g. 14-07-13-CrabTest
# ODIR:	   parent directory of specific runparameter variation 
# WORKDIR: directory within ODIR for the number of identical realizations
# if no runparameter file: ODIR= RUNDIR
# if realizationNumber = 1: WORKDIR = ODIR

###############################################################################
# clean up output directory and create subfolders for each run:

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter

# make temporary directory for each realisation:
if ( $onlyOneRunBool == "0" ) then
	set WORKDIR=$ODIR/realisation-${realisationnumber}
	mkdir -p $WORKDIR
	cd $WORKDIR
else
	set WORKDIR=$ODIR # = current directory"$(pwd)"
	cd $WORKDIR
endif

###############################################################################
# modify clumpy parameter file:
echo " *** modify clumpy parameter file: ***"
sed  's/EEEEZ/'$alphaIntDeg'/' $SCRIPTDIR/dmClumps.clumpy_params.txt > $ODIR/Inputfiles/tmp1.txt
sed	 's/GGGGZ/'$gSIMU_IS_ANNIHIL_OR_DECAY'/' $ODIR/Inputfiles/tmp1.txt > $ODIR/Inputfiles/tmp2.txt
sed  's/HHHHZ/'$gGAL_TOT_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp2.txt > $ODIR/Inputfiles/tmp3.txt
sed  's/IIIIZ/'$gGAL_TOT_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp3.txt > $ODIR/Inputfiles/tmp4.txt
sed  's/JJJJZ/'$gGAL_TOT_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp4.txt > $ODIR/Inputfiles/tmp5.txt
sed  's/KKKKZ/'$gGAL_TOT_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp5.txt > $ODIR/Inputfiles/tmp6.txt
sed  's/LLLLZ/'$gGAL_TOT_RSCALE'/' $ODIR/Inputfiles/tmp6.txt > $ODIR/Inputfiles/tmp7.txt
sed  's/MMMMZ/'$gGAL_RHOSOL'/' $ODIR/Inputfiles/tmp7.txt > $ODIR/Inputfiles/tmp8.txt
sed  's/NNNNZ/'$gGAL_RSOL'/' $ODIR/Inputfiles/tmp8.txt > $ODIR/Inputfiles/tmp9.txt
sed  's/OOOOZ/'$gGAL_RVIR'/' $ODIR/Inputfiles/tmp9.txt > $ODIR/Inputfiles/tmp10.txt
sed  's/PPPPZ/'$gGAL_DPDV_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp10.txt > $ODIR/Inputfiles/tmp11.txt
sed  's/QQQQZ/'$gGAL_DPDV_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp11.txt > $ODIR/Inputfiles/tmp12.txt
sed  's/SSSSZ/'$gGAL_DPDV_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp12.txt > $ODIR/Inputfiles/tmp13.txt
sed  's/TTTTZ/'$gGAL_DPDV_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp13.txt > $ODIR/Inputfiles/tmp14.txt
sed  's/UUUUZ/'$gGAL_DPDV_RSCALE'/' $ODIR/Inputfiles/tmp14.txt > $ODIR/Inputfiles/tmp15.txt
sed  's/VVVVZ/'$gGAL_DPDM_SLOPE'/' $ODIR/Inputfiles/tmp15.txt > $ODIR/Inputfiles/tmp16.txt
sed  's/WWWWZ/'$gDM_MMIN_SUBS'/' $ODIR/Inputfiles/tmp16.txt > $ODIR/Inputfiles/tmp17.txt
sed  's/XXXXZ/'$gGAL_SUBS_N_INM1M2'/' $ODIR/Inputfiles/tmp17.txt > $ODIR/Inputfiles/tmp18.txt
sed  's/YYYYZ/'$gGAL_CLUMPS_FLAG_PROFILE'/' $ODIR/Inputfiles/tmp18.txt > $ODIR/Inputfiles/tmp19.txt
sed  's/ZZZZX/'$gGAL_CLUMPS_SHAPE_PARAMS_0'/' $ODIR/Inputfiles/tmp19.txt > $ODIR/Inputfiles/tmp20.txt
sed  's/ZAAAZ/'$gGAL_CLUMPS_SHAPE_PARAMS_1'/' $ODIR/Inputfiles/tmp20.txt > $ODIR/Inputfiles/tmp21.txt
sed  's/ZBBBZ/'$gGAL_CLUMPS_SHAPE_PARAMS_2'/' $ODIR/Inputfiles/tmp21.txt > $ODIR/Inputfiles/tmp22.txt
sed  's/ZCCCZ/'$gGAL_CLUMPS_FLAG_CVIRMVIR'/' $ODIR/Inputfiles/tmp22.txt > $ODIR/Inputfiles/tmp23.txt
sed  's/ZDDDZ/'$gDM_MMAXFRAC_SUBS'/' $ODIR/Inputfiles/tmp23.txt > $ODIR/Inputfiles/tmp24.txt
sed  's/ZEEEZ/'$gDM_RHOSAT'/' $ODIR/Inputfiles/tmp24.txt > $ODIR/Inputfiles/tmp25.txt
sed  's/ZFFFZ/'$gGAL_SUBS_M1'/' $ODIR/Inputfiles/tmp25.txt > $ODIR/Inputfiles/tmp26.txt
sed  's/ZGGGZ/'$gGAL_SUBS_M2'/' $ODIR/Inputfiles/tmp26.txt > $ODIR/Inputfiles/tmp27.txt
sed  's/ZHHHZ/'$seed'/' $ODIR/Inputfiles/tmp27.txt > $ODIR/Inputfiles/tmp28.txt
sed  's/ZIIIZ/'$gLIST_HALOES'/' $ODIR/Inputfiles/tmp28.txt > $ODIR/Inputfiles/dmClumps.clumpy_params.txt
rm $ODIR/Inputfiles/tmp*

rm skymap.fits

# copy clumpy binary into Inputfiles directory:
cp $CLUMPY/bin/clumpy $DDIR

chmod u+x $ODIR/Inputfiles/dmClumps.clumpy_params.txt

###############################################################################
# start clumpy routine:
echo " *** start clumpy routine: ***"
if ( $gLIST_Bool == "1" ) then
	$DDIR/clumpy -gp8 $ODIR/Inputfiles/dmClumps.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse 1 | tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
else
	$DDIR/clumpy -gp7 $ODIR/Inputfiles/dmClumps.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse | tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
endif
mv -f  -v $DDIR/* $WORKDIR/
rm *.drawn
echo

###############################################################################
# transform clumpy output file into healpix format:
echo " *** transform clumpy output file into healpix format and prepare power spectrum calculation: ***"
python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDegGal -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $WORKDIR -j Jtot  -z fits -m $gDM_MMIN_SUBS -d $gLIST_Bool

###############################################################################
# evaluate clumpy output spectrum with anafast routine:	
echo " *** evaluate clumpy output spectrum with anafast routine: ***"
cd $ODIR/
mkdir -p spectra-clumpy

cd $HEALPIX 
if ( $onlyOneRunBool == "0" ) then
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_pseudo.par | tee $ODIR/Logfiles/module1-anafast_pseudo-realisation-${realisationnumber}.log
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_mask.par | tee $ODIR/Logfiles/module1-anafast_mask-realisation-${realisationnumber}.log
else
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_pseudo.par | tee $ODIR/Logfiles/module1-anafast_pseudo.log
	$HEALPIX/binfortran/anafast -s $WORKDIR/anafast_mask.par | tee $ODIR/Logfiles/module1-anafast_mask.log
endif

###############################################################################
# rename files:

cd $WORKDIR/
rm skymap.healpix
mv *.healpix skymap.healpix
mv *.spectrum_pseudo.fits skymap.spectrum_pseudo.fits
mv *.spectrum_mask.fits skymap.spectrum_mask.fits

###############################################################################
# convert spectra fits files to simple text files for deconvolution:

python $SCRIPTDIR/dmClumps.convertFitsSpectra.py -i $WORKDIR/skymap.spectrum_pseudo.fits -o $WORKDIR/skymap.spectrum_pseudo.txt
python $SCRIPTDIR/dmClumps.convertFitsSpectra.py -i $WORKDIR/skymap.spectrum_mask.fits -o $WORKDIR/skymap.spectrum_mask.txt

###############################################################################
# Deconvolution:

# get length of cl vectors:

set clmax = `cat $WORKDIR/skymap.spectrum_pseudo.txt | wc -l`
set clmax = `expr $clmax - 1`
echo -n " cl_max  for deconvolution is: " $clmax "\n"

# Deconvolve!
/afs/ifh.de/group/cta/scratch/mhuetten/Programs/deconvolve_mask/deconvolve_mask $clmax $WORKDIR/skymap.spectrum_pseudo.txt $WORKDIR/skymap.spectrum_mask.txt

###############################################################################
# clean up: 

rm anafast_pseudo.par
rm anafast_mask.par

# move output spectra into parent folder:
#mv *.spectrum $ODIR/spectra-clumpy/clumpy-realisation-${realisationnumber}.spectrum

rm *.fits
rm -f $DDIR/*

##sleep 20

##exit
