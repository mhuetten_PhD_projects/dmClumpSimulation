#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Generating skymap script for clumpy Simulation:"
	echo
	echo "dmClumps.sub.sh <output directory>  [plot or fits  [ [Jsmooth, Jclumps, Jsum or Jtot] [plotview]  [lower exponent] [upper exponent] [singlerun]]" 
	echo
	echo "  - if no optional arguments are given, a data file will be written and Jtot will be plotted in cartesian projection. "
	echo "  - if the option fits is given, all further arguments won't be considered (and can also be omitted)"
	echo "  - if the option plot is given with no further arguments, Jtot is plotted in cartesian projection"
	echo "  plotview is either 'cart' (cartesian projection) or 'moll' (mollweide) projection "
	echo "  Jsmooth: smooth component, Jclumps: only drawn clumps (Jdrawn), Jsum: Jsmooth +  <Jsub> + Jcrossprod "
	echo "  lower/upper exponent: plotting range for logarithmic plot "
	echo "  command 'plot' plots only skymap (but of course only if .healpix file has been already created before), 'fits' creates only .healpix file without plotting."
	echo 

   exit
fi



ODIR=$1

FITSORPLOT=dummydummy
if [ -n "$2" ]
then
FITSORPLOT=$2
fi

JCHOICE=Jtot
if [ -n "$3" ]
then
JCHOICE=$3
fi


PLOTVIEW=cart
if [ -n "$4" ]
then
PLOTVIEW=$4
fi

LOWER=15
if [ -n "$5" ]
then
LOWER=$5
fi

UPPER=18
if [ -n "$6" ]
then
UPPER=$6
fi

SINGLERUN=dummydummy
if [ -n "$7" ]
then
SINGLERUN=$7
fi


# Scriptdirectory:
SCRIPTDIR="/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation"

#############################################
#read variables from parameter files:

parameterfile=${ODIR}/Inputfiles/dmClumps.parameters


#read variables from parameter file:

# CLUMPY technical parameters:
export alphaIntDeg=$(cat $parameterfile 	| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $parameterfile		| head -n22 | tail -n1 | sed -e 's/^[ \t]*//') 
export gSIMU_SEED=$(cat $parameterfile			| head -n25 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $parameterfile 		| head -n31 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $parameterfile 	| head -n34 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $parameterfile 	| head -n37 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $parameterfile	| head -n40 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $parameterfile | head -n46 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $parameterfile 	| head -n51 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $parameterfile | head -n54 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $parameterfile | head -n55 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $parameterfile | head -n56 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $parameterfile 		| head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $parameterfile			 	| head -n64 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $parameterfile 			| head -n67 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $parameterfile 				| head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $parameterfile 				| head -n73 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $parameterfile 	 | head -n79 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $parameterfile | head -n82 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $parameterfile | head -n83 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $parameterfile | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $parameterfile		 | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $parameterfile   | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $parameterfile | head -n96 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $parameterfile | head -n97 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $parameterfile | head -n98 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $parameterfile  | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps mass distribution properties:
export gGAL_DPDM_SLOPE=$(cat $parameterfile			 | head -n108 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $parameterfile		 | head -n111 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $parameterfile			 | head -n114 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMAXFRAC_SUBS=$(cat $parameterfile 		 | head -n117 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $parameterfile			 | head -n120 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $parameterfile 			 | head -n123 | tail -n1 | sed -e 's/^[ \t]*//')

# Draw object from list?
export gLIST_Bool=$(cat $parameterfile		| head -n128 | tail -n1 | sed -e 's/^[ \t]*//')
export gLIST_HALOES=$(cat $parameterfile	| head -n132 | tail -n1 | sed -e 's/^[ \t]*//')

# If a run parameter file exists:
if [ -e "${ODIR}/Inputfiles/dmClumps.runparameter" ]
then
	echo " evaluate multi run simulation"
	PLIST=${ODIR}/Inputfiles/dmClumps.runparameter
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n " Running parameter is: "
	echo $runparameter


	# make the script prepared to be run on batch: there, only the corresponding run
	# should be evaluated:
	if  [ "$FITSORPLOT" = "fits" ]
	then	
		echo " only evaluating parameter run $runparameter = $SINGLERUN"

		case $runparameter in
		"user_rse") user_rse=$SINGLERUN;;
		"alphaIntDeg") alphaIntDeg=$SINGLERUN;;
		"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$SINGLERUN;;
		"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$SINGLERUN;;
		"gGAL_RHOSOL") gGAL_RHOSOL=$SINGLERUN;;
		"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$SINGLERUN;;
		"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$SINGLERUN;;
		"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$SINGLERUN;;
		esac

		# rename $runname into:	
		subrunname=$ODIR/$runparameter-$SINGLERUN
	
		python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $subrunname  -j $JCHOICE -s $PLOTVIEW -l $LOWER -k $UPPER -z $FITSORPLOT -d $gLIST_Bool -b $gLIST_HALOES -g $gGAL_DPDV_FLAG_PROFILE

	else
	for AFIL in $PARAMETERS
	do

	echo " now evaluating parameter run $runparameter = $AFIL"

	case $runparameter in
	"user_rse") user_rse=$AFIL;;
	"alphaIntDeg") alphaIntDeg=$AFIL;;
	"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
	"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
	"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
	"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
	"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
	"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
	esac

	# rename $runname into:	
	subrunname=$ODIR/$runparameter-$AFIL
	
	python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $subrunname  -j $JCHOICE -s $PLOTVIEW -l $LOWER -k $UPPER -z $FITSORPLOT -m $gDM_MMIN_SUBS -c $gGAL_CLUMPS_FLAG_CVIRMVIR -d $gLIST_Bool -b $gLIST_HALOES -g $gGAL_DPDV_FLAG_PROFILE  &

	done
	fi
fi

if [ ! -e "${ODIR}/Inputfiles/dmClumps.runparameter" ]
then

cd $ODIR
if [ -d "realisation-1" ]; then
	echo " evaluating first three runs out of a multiple sample"
	for realisation in 1 2 3
	do
	WORKDIR=$ODIR/realisation-$realisation
	python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $WORKDIR  -j $JCHOICE -s $PLOTVIEW -l $LOWER -k $UPPER -z $FITSORPLOT -m $gDM_MMIN_SUBS -c $gGAL_CLUMPS_FLAG_CVIRMVIR -d $gLIST_Bool -b $gLIST_HALOES -g $gGAL_DPDV_FLAG_PROFILE &
	done
else
	WORKDIR=$ODIR
	echo " evaluating single run"
python $SCRIPTDIR/dmClumps.skymap.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $WORKDIR  -j $JCHOICE -s $PLOTVIEW -l $LOWER -k $UPPER -z $FITSORPLOT -m $gDM_MMIN_SUBS -c $gGAL_CLUMPS_FLAG_CVIRMVIR -d $gLIST_Bool -b $gLIST_HALOES  -g $gGAL_DPDV_FLAG_PROFILE
	
fi
cd $SCRIPTDIR


fi
exit

