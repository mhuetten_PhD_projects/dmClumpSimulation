#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Generating skymap script for clumpy Simulation:"
	echo
	echo "dmClumps.sub.sh <output directory>  [plot or fits  [ [Jtot or Jclumps] [plotview]  [lower exponent] [upper exponent] ]" 
	echo
	echo "  - if no optional arguments are given, a data file will be written and Jtot will be plotted in cartesian projection. "
	echo "  - if the option fits is given, all further arguments won't be considered (and can also be omitted)"
	echo "  - if the option plot is given with no further arguments, Jtot is plotted in cartesian projection"
	echo "  plotview is either 'cart' (cartesian projection) or 'moll' (mollweide) projection "
	echo "  lower/upper exponent: plotting range for logarithmic plot "
	echo "  command 'plot' plots only skymap (but of course only if .healpix file has been already created before), 'fits' creates only .healpix file without plotting."
	echo 

   exit
fi

outdir=$1





# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"

#############################################
#read variables from parameter files:

# This is hardcoded for convenience (for not needing to input each time the full path ouf the outputdirectory)

SCRIPTDIR=/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation
WORKDIR=/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation

parameterfile=${WORKDIR}/${outdir}/Inputfiles/dmClumps.parameters
PLIST=${WORKDIR}/${outdir}/Inputfiles/dmClumps.runparameter



export psiZeroDeg=$(cat $parameterfile | head -n2 | tail -n1)
export thetaZeroDeg=$(cat $parameterfile | head -n4 | tail -n1)
export psiWidthDeg=$(cat $parameterfile | head -n6 | tail -n1)
export thetaWidthDeg=$(cat $parameterfile | head -n8 | tail -n1)
export alphaIntDeg=$(cat $parameterfile | head -n10 | tail -n1)
export user_rse=$(cat $parameterfile | head -n12 | tail -n1)
export runnumber=$(cat $parameterfile | head -n14 | tail -n1)




###############################################################################################################
# number of parameters:
PARAMETERS=`cat $PLIST`



#
#########################################
# loop over all files in files loop
for AFIL in $PARAMETERS
do
   echo "now evaluating parameter run $AFIL"
python $SCRIPTDIR/dmClumps.mergespectra.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $AFIL -o $WORKDIR -r $outdir &
#$TOYDIR/clumpyMC.prepareSkymap.py 
done

exit

