import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import *
import pylab


print 'load data'
data = np.loadtxt('/afs/ifh.de/group/cta/scratch/mhuetten/workdata/dmClumpSimulation/20degreesCloserToCenterDelta0.01deg/annihil_gal2D_LOS12,0_FOV20x10_rse5_alphaint0.01deg.dat')
# 

datalength = len(data[:,1])
print " this gives", datalength, "pixels in clumpy map"
nPsi = 1
for j in range(1,datalength):
    if data[j,0]!= data[j-1,0]:
        nPsi = nPsi+1
print " ... thereof in psi direction:",nPsi,"pixels"
nTheta = datalength/nPsi
print " ... thereof in in theta direction:",nTheta,"pixels"
print ""

# extract psi, theta vector:
psiVector = np.zeros(nPsi)
thetaVector = np.zeros(nTheta)
for j in range(nPsi):
    psiVector[j] = data[nTheta*j,0]
for k in range(nTheta):    
    thetaVector[k] = data[k,1]

MAX = max(data[:,6])

x = np.zeros(datalength)
y = np.zeros(datalength)
z = np.zeros(datalength)
# 
plotdata = np.zeros(shape=(nTheta,nPsi))
for k in range(nTheta):
        for j in range(nPsi): 
            plotdata[k,j] =  data[k + j*nTheta,6]
#colormap_r = ListedColormap(Blues.colors[::-1])
   
fig = plt.imshow(plotdata,norm=LogNorm(vmin=10**8, vmax=10**12) )#, colormap='gist_earth')
#ax.set_axis_off()
#pylab.savefig('foopink.png', bbox_inches=0)
fig.set_cmap('bone')
plt.axis('off')
plt.show()
