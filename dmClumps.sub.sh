#!/bin/sh
#
# script run clumpy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Submission script for clumpy Simulation:"
	echo
	echo "dmClumps.sub.sh <dmClumps.parameters> [number of simulations] [developing mode] [dmClumps.runparameter]"
	echo
	echo "  for the dmClumps.parameters file use template without changing line numbers"
	echo "  in case of choosing a varying parameter in a dmClumps.runparameter file,"
	echo "  write name of parameter in first line, then al values you want to compute."
	echo "  Example:"
	echo
	echo "    user_rse"
	echo "    5.0"
	echo "    10.0"
	echo "    15.0"
	echo
	
	exit
fi

#read variables from parameter file:

# General parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')

# CLUMPY technical parameters:
export alphaIntDeg=$(cat $1 	| head -n19 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $1 		| head -n22 | tail -n1 | sed -e 's/^[ \t]*//') 
export gSIMU_SEED=$(cat $1 			| head -n25 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $1 		| head -n31 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $1 	| head -n34 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $1 	| head -n37 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $1 	| head -n40 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $1 | head -n46 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $1 	| head -n51 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $1 | head -n54 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $1 | head -n55 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $1 | head -n56 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $1 		| head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $1			 	| head -n64 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $1 			| head -n67 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $1 				| head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $1 				| head -n73 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $1 	 | head -n79 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $1 | head -n82 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $1 | head -n83 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $1 | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $1		 | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $1   | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $1 | head -n96 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $1 | head -n97 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $1 | head -n98 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $1  | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps mass distribution properties:
export gGAL_DPDM_SLOPE=$(cat $1			 | head -n108 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $1		 | head -n111 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $1			 | head -n114 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMAXFRAC_SUBS=$(cat $1 		 | head -n117 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $1 			 | head -n120 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $1 			 | head -n123 | tail -n1 | sed -e 's/^[ \t]*//')

# Draw object from list?
export gLIST_Bool=$(cat $1		| head -n128 | tail -n1 | sed -e 's/^[ \t]*//')
export gLIST_HALOES=$(cat $1	| head -n132 | tail -n1 | sed -e 's/^[ \t]*//')

#echo $runname
#echo $outdir
#echo $user_rse
#echo $alphaIntDeg
#echo $psiZeroDeg
#echo $thetaZeroDeg
#echo $psiWidthDeg
#echo $thetaWidthDeg
#echo $gSIMU_IS_ANNIHIL_OR_DECAY
#echo $gGAL_TOT_FLAG_PROFILE
#echo $gGAL_TOT_SHAPE_PARAMS_0
#echo $gGAL_TOT_SHAPE_PARAMS_1
#echo $gGAL_TOT_SHAPE_PARAMS_2
#echo $gGAL_TOT_RSCALE
#echo $gGAL_RHOSOL
#echo $gGAL_RSOL
#echo $gGAL_RVIR
#echo $gGAL_DPDV_FLAG_PROFILE
#echo $gGAL_DPDV_SHAPE_PARAMS_0
#echo $gGAL_DPDV_SHAPE_PARAMS_1
#echo $gGAL_DPDV_SHAPE_PARAMS_2
#echo $gGAL_DPDV_RSCALE
#echo $gGAL_DPDM_SLOPE
#echo $gDM_MMIN_SUBS
#echo $gGAL_SUBS_N_INM1M2
#echo $gGAL_CLUMPS_FLAG_PROFILE
#echo $gGAL_CLUMPS_SHAPE_PARAMS_0
#echo $gGAL_CLUMPS_SHAPE_PARAMS_1
#echo $gGAL_CLUMPS_SHAPE_PARAMS_2
#echo $gGAL_CLUMPS_FLAG_CVIRMVIR

if [ -n "$4" ]
then
	PLIST=$4
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
fi

# number of repeated realisations:
samplenumber=1
if [ -n "$2" ]; then
samplenumber=$2
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

# Make Logfile directory for submission scripts and batch logfiles output:
cd $SCRATCH/LOGS/DMCLUMPS-SIMULATION
#mkdir -p $DATE
QLOG=$SCRATCH/LOGS/DMCLUMPS-SIMULATION/ #$DATE

# developing mode?
devmode=0
if [ -n "$3" ]; then
devmode=$3
fi
if [[ $devmode == "0" ]]; then
	computingTime=47:59:59
	memorySize=12G
elif [[ $devmode == "1" ]]; then
	computingTime=00:29:59
	memorySize=4G
else
	echo "EXIT: devmode variable must be either 0 or 1"
	echo
	exit
fi		


cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"
# now forget outdir

# skeleton script
FSCRIPT="dmClumps.qsub"


###############################################################################
# If a runparameter file is parsed:
###############################################################################

if [ -n "$4" ]; then
		cd $RUNDIR
		rm -r Inputfiles
		mkdir -p Inputfiles
		cd $SCRIPTDIR
		cp dmClumps.* $RUNDIR/Inputfiles/
		rm $RUNDIR/Inputfiles/dmClumps.clumpy_params.txt
	
	# now loop over all files in files loop:
	
	for AFIL in $PARAMETERS
	do
		echo "now starting parameter run $runparameter = $AFIL"
		
		# create output directories:   
		cd $RUNDIR
		mkdir $runparameter-$AFIL
		cd $runparameter-$AFIL
		ODIR="$(pwd)"
		rm -r Logfiles
		mkdir -p Logfiles
		mkdir -p Inputfiles
		cd $SCRIPTDIR
		cp dmClumps.* $RUNDIR/Inputfiles/
		rm $RUNDIR/Inputfiles/dmClumps.clumpy_params.txt
	
		# output directory for error/output from batch system
		# in case you submit a lot of scripts: QLOG=/dev/null
		
		case $runparameter in
		"user_rse") user_rse=$AFIL;;
		"alphaIntDeg") alphaIntDeg=$AFIL;;
		"gDM_MMIN_SUBS") gDM_MMIN_SUBS=$AFIL;;
		"gGAL_DPDM_SLOPE") gGAL_DPDM_SLOPE=$AFIL;;
		"gGAL_RHOSOL") gGAL_RHOSOL=$AFIL;;
		"gGAL_SUBS_N_INM1M2") gGAL_SUBS_N_INM1M2=$AFIL;;
		"gGAL_CLUMPS_FLAG_PROFILE") gGAL_CLUMPS_FLAG_PROFILE=$AFIL;;
		"gGAL_DPDV_FLAG_PROFILE") gGAL_DPDV_FLAG_PROFILE=$AFIL;;
			esac

		for i in $(seq 1 1 $samplenumber)
		do
			if [[ $samplenumber == "1" ]]; then
				FNAM="$QLOG/dmClumps-$runname-$runparameter-$AFIL"
				onlyOneRunBool=1
			else 
				FNAM="$QLOG/dmClumps-$runname-$runparameter-$AFIL-$i"
				onlyOneRunBool=0
				echo "now starting sample realisation number $i"
			fi
								
			sed -e "s|FFFFF|$user_rse|" \
				-e "s|PEEED|$QLOG|" \
				-e "s|OODIR|$ODIR|" \
				-e "s|DAATE|$DATE|" \
				-e "s|MMMMM|$gGAL_RHOSOL|" \
				-e "s|NNNNN|$gGAL_RSOL|" \
				-e "s|OOOOO|$gGAL_RVIR|" \
				-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
				-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
				-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
				-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
				-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
				-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
				-e "s|WWWWW|$gDM_MMIN_SUBS|" \
				-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
				-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
				-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
				-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
				-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
				-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
				-e "s|AAAAA|$psiZeroDeg|" \
				-e "s|BBBBB|$thetaZeroDeg|" \
				-e "s|CCCCC|$psiWidthDeg|" \
				-e "s|DDDDD|$thetaWidthDeg|" \
				-e "s|EEEEE|$alphaIntDeg|" \
				-e "s|RRRRR|$AFIL|" \
				-e "s|RUUUN|$RUNDIR|" \
				-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
				-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
				-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
				-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
				-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
				-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
				-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
				-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
				-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
				-e "s|ZEEEE|$gDM_RHOSAT|" \
				-e "s|ZFFFF|$gGAL_SUBS_M1|" \
				-e "s|ZGGGG|$gGAL_SUBS_M2|" \
				-e "s|ZHHHH|$gSIMU_SEED|" \
				-e "s|ZIIII|$gLIST_HALOES|" \
				-e "s|ZJJJJ|$gLIST_Bool|" \
				-e "s|ZKKKK|$i|" \
				-e "s|ZLLLL|$onlyOneRunBool|" \
				-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
			
			chmod u+x $FNAM.sh
			echo "script name is:" $FNAM.sh
			
			if [[ $devmode == "1" ]]; then
				echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
			fi
			
			qsub -V -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ "$FNAM.sh"
			echo
		done
	done
fi

###############################################################################
# If NO runparameter file exists:
###############################################################################

if [ ! -n "$4" ]; then
	echo "now submitting single job to batch"
	# RUNDIR is equal ODIR in this case!
	
	AFIL=singlerundummy
	
	# AFIL is not used in this case!

	cd $RUNDIR
	ODIR="$(pwd)"
	rm -r Logfiles
	rm -r Inputfiles
		
	mkdir -p Inputfiles
	mkdir -p Logfiles
		
	cd $SCRIPTDIR
	cp dmClumps.* $ODIR/Inputfiles/
	rm $ODIR/Inputfiles/dmClumps.clumpy_params.txt
	rm $ODIR/Inputfiles/dmClumps.runparameter
		

	for i in  $(seq 1 1 $samplenumber)
	do
		realisationnumber=$i # this rename has just been done because of some buggy reading of the sed command...
		
		if [[ $samplenumber == "1" ]]; then
			FNAM="$QLOG/dmClumps-$runname"
			onlyOneRunBool=1
		else 
			FNAM="$QLOG/dmClumps-$runname-$i"
			onlyOneRunBool=0
			echo "now starting sample realisation number $i"
		fi
						
		sed -e "s|FFFFF|$user_rse|" \
			-e "s|PEEED|$QLOG|" \
			-e "s|OODIR|$ODIR|" \
			-e "s|DAATE|$DATE|" \
			-e "s|MMMMM|$gGAL_RHOSOL|" \
			-e "s|NNNNN|$gGAL_RSOL|" \
			-e "s|OOOOO|$gGAL_RVIR|" \
			-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
			-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
			-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
			-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
			-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
			-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
			-e "s|WWWWW|$gDM_MMIN_SUBS|" \
			-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
			-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
			-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
			-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
			-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
			-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
			-e "s|AAAAA|$psiZeroDeg|" \
			-e "s|BBBBB|$thetaZeroDeg|" \
			-e "s|CCCCC|$psiWidthDeg|" \
			-e "s|DDDDD|$thetaWidthDeg|" \
			-e "s|EEEEE|$alphaIntDeg|" \
			-e "s|RRRRR|$AFIL|" \
			-e "s|RUUUN|$RUNDIR|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
			-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
			-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
			-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
			-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
			-e "s|ZEEEE|$gDM_RHOSAT|" \
			-e "s|ZFFFF|$gGAL_SUBS_M1|" \
			-e "s|ZGGGG|$gGAL_SUBS_M2|" \
			-e "s|ZHHHH|$gSIMU_SEED|" \
			-e "s|ZIIII|$gLIST_HALOES|" \
			-e "s|ZJJJJ|$gLIST_Bool|" \
			-e "s|ZKKKK|$i|" \
			-e "s|ZLLLL|$onlyOneRunBool|" \
			-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT.sh > $FNAM.sh
	
		chmod u+x $FNAM.sh
		echo "script name is:" $FNAM.sh
		
		if [[ $devmode == "1" ]]; then
			echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
		fi
	
		qsub -V -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_vmem=$memorySize -l tmpdir_size=$memorySize -o $QLOG/ -e $QLOG/ "$FNAM.sh"
		echo
	done
fi
exit
