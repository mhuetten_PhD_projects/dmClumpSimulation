#============= INSTRUCTIONS =================
# Purpose: import clump data from '....drawn' to 'list_example.txt'
# input: decay...........drawn
# output: list_example.txt

# Import all libraries needed 
import pandas as pd
import os, glob
import sys
import getopt
import warnings
import numpy as np
import bisect

def main(argv):
	
	###########################################################################
	#  read input variables:
	
	infile1 = 'empty'

	try:
	    opts, args = getopt.getopt(sys.argv[1:],"i:j:o:",["infile11=","infile12=","outfile="])
	except getopt.GetoptError:
	    print('Wrong input. The input options are:')
	    print('-i or --infile1 for reading the input FITS file')
	    sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
		    # help option
			print('The input options are:')
			print('-i or --infile1 for reading the input FITS file')
			sys.exit()
		elif opt in ("-i", "--infile11"):
		    infile1 = str(arg)
		elif opt in ("-j", "--infile12"):
		    infile2 = str(arg)
		elif opt in ("-o", "--outfile"):
			outfile = str(arg)

	
	if infile1 == 'empty':
	    print('  Please parse an input file')
	    sys.exit()

	print "This is merge.py"

	#Locate the data
	data_clumps = infile1
	
	#Assign the name of each column
	columnname_old=['clump', 'long', 'lat', 'mass', 'rvir', 'd', 'z', 'rhos', 'rs', 'alpha', 'beta', 'gamma', 'profile', 'J', 'JJcon']
	columnname_new=['clump', 'long', 'lat', 'mass', 'rvir', 'Mtid', 'Rtid', 'Mequdens', 'Requdens', 'Dgal', 'd', 'z', 'rhos', 'rs', 'alpha', 'beta', 'gamma', 'profile', 'J', 'JJcon']
	
	#Import the data
	try:
		df = pd.read_csv(data_clumps, header=None, comment='#', sep = r"\s*", names=columnname_old)
		is_new = False
	except:
		df = pd.read_csv(data_clumps, header=None, comment='#', sep = r"\s*", names=columnname_new)
		is_new = True
		#Delete the unwanted columns
		
	if is_new:
		print "Extended infos in drawn file detected!"
		
	df = df.ix[4:]

	# get number of clumps:
	JJcon  = df.as_matrix(columns=['JJcon'])
	JJcon  = np.asarray(JJcon[:,0]).astype(np.float)
	nclumps = len(JJcon)
	print "number of clumps:", nclumps
	
	#Delete the unwanted columns
	del df['z']
	del df['beta']
	del df['gamma']
	del df['profile']
	del df['J']
	del df['JJcon']


	
	# read the alphaint list:
	data_alphaint = infile2
	
	alpha_int_list = np.loadtxt(data_alphaint)
	
	n_alphaint = len(alpha_int_list)/nclumps
	
	# get alphaint values:
	alphaint_vals = np.zeros(n_alphaint)
	for i in range(n_alphaint):
		alphaint_vals[i] = alpha_int_list[i,0]
	
	print "alpha_int values:", alphaint_vals
	j_01 = bisect.bisect(alphaint_vals, 0.1) - 1
	j_001 = bisect.bisect(alphaint_vals, 0.01) - 1
	j_003 = bisect.bisect(alphaint_vals, 0.03) - 1
	j_005 = bisect.bisect(alphaint_vals, 0.05) - 1
	j_008 = bisect.bisect(alphaint_vals, 0.8) - 1
	j_05 = bisect.bisect(alphaint_vals, 0.5) - 1
	print "index of alpha_int=0.1deg:", j_01
	print "corresponding alpha_int:", alphaint_vals[j_01] 
	
	alphaint_matrix = np.zeros(shape=(nclumps, n_alphaint))
	
	J_half = np.zeros(nclumps)
	J_tot = np.zeros(nclumps)
	J_best = np.zeros(nclumps)
	J_01 = np.zeros(nclumps)
	J_001 = np.zeros(nclumps)
	J_003 = np.zeros(nclumps)
	J_005 = np.zeros(nclumps)
	J_008 = np.zeros(nclumps)
	J_05 = np.zeros(nclumps)
	alphaint_half = np.zeros(nclumps)
	alphaint_best = np.zeros(nclumps)
	sigratio_half = np.zeros(nclumps)
	sigratio_best = np.zeros(nclumps)
	sig_half_check = np.zeros(nclumps)
	sig_best_check = np.zeros(nclumps)
	

	Jfactor_column = 1 # = 1 for no subsub contribution, =4 for subsub contrib.
	JJmax_column = 2 # = 2 for no subsub contribution, =8 for subsub contrib.
	
	for i in range(nclumps):
		significance = np.zeros(n_alphaint)
		startentry = i * n_alphaint
		Jfactors = alpha_int_list[i * n_alphaint:(i + 1) * n_alphaint, Jfactor_column]
		
		J_01[i] = Jfactors[j_01]
		J_001[i] = Jfactors[j_001]
		J_003[i] = Jfactors[j_003]
		J_005[i] = Jfactors[j_005]
		J_008[i] = Jfactors[j_008]
		J_05[i] = Jfactors[j_05]
		
		print "Jfactors:"
		print Jfactors
		print ""
		
		significance = Jfactors/alphaint_vals
		print "significance:"
		print significance
		print ""
		
		# get alphint_half:
		for j in range(n_alphaint):
			J_half[i] = Jfactors[j]
			alphaint_half[i] = alphaint_vals[j]
			j_half = j
			if alpha_int_list[i * n_alphaint + j, JJmax_column] > 0.5: # compare to y_norm at alpha_int_max
				break
		# get J_tot:
		J_tot[i] = Jfactors[n_alphaint-1]
		
		# get alphint_best:
		for j in range(n_alphaint-1):
			J_best[i] = Jfactors[j]
			alphaint_best[i] = alphaint_vals[j]
			j_best = j
			if significance[j+1] < significance[j]:
				break
		
		print "index of alpha_int=0.1deg:", j_01
		print "alpha_int=0.1deg:", alphaint_vals[j_01]
		print "index of alpha_int=0.5deg:", j_05
		print "alpha_int=0.5deg:", alphaint_vals[j_05]
		print "index of alpha_int_half:", j_half
		print "index of alpha_int_best:", j_best
		
		# get ratios to alphaint=0.1deg:
		sig_01 = significance[j_01]
		sig_best = significance[j_best]
		sig_half = significance[j_half]
		sigratio_half[i] = sig_half/sig_01
		sigratio_best[i] = sig_best/sig_01
		sig_best_check[i] = J_best[i] / alphaint_best[i]
		sig_half_check[i] = J_half[i] / alphaint_half[i]
		
		print "check sig_Best:", sig_best, sig_best_check[i]
		print "check sig_half:",sig_half, sig_half_check[i]
		print "check sig_01:",sig_01, J_01[i]/0.1
			
		alphaint_matrix[i,:] = Jfactors
		
		#Add the Jfactor columnsn at the end
	df['alpha_half'] = alphaint_half.tolist()
	df['J_half'] = J_half.tolist()
	df['alpha_best'] = alphaint_best.tolist()
	df['J_best'] = J_best.tolist()
	df['J_01'] = J_01.tolist()
	df['sigratio_half'] = sigratio_half.tolist()
	df['sigratio_best'] = sigratio_best.tolist()
	df['J_001'] = J_001.tolist()
	df['J_05'] = J_05.tolist()
	df['JJcon_005'] = JJcon.tolist()
	df['J_005'] = J_005.tolist()
	df['J_08'] = J_008.tolist()
	df['J_tot'] = J_tot.tolist()
	df['J_003'] = J_003.tolist()
	
	#Define the output formats
	fltfm = lambda x: '%7.1f' %x 
	scifm = lambda x: '%10.2E' %x
	strfm = lambda x: '%5s' %x
	intfm = lambda x: '%-2d' %x
	
	if is_new:
		fm = {'long': fltfm, 'lat': fltfm, 'mass': scifm, 'rvir': scifm, 'Mtid': scifm, 'Rtid': scifm, 'Mequdens': scifm, 'Requdens': scifm, 'Dgal': scifm, 'd': scifm,  'rhos': scifm, 'rs': scifm}
	else:	
		fm = {'long': fltfm, 'lat': fltfm, 'mass': scifm, 'rvir': scifm, 'd': scifm,  'rhos': scifm, 'rs': scifm}

	#Write into the file
	with open(outfile, 'w') as f: 
		df.to_string(f, header=True, col_space=5, index=False, formatters=fm)
	f.close()
	
	
	print "merge.py finished"
	
if __name__ == "__main__":
     
	main(sys.argv[1:])

##    end of file    ##########################################################
############################################################################### 
	
