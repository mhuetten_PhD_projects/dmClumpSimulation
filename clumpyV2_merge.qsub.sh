#$ -S /bin/tcsh
#
# script to perform Dark Matter clumpy simulations
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

set PARAMETER=RRRRR
set RUNDIR=RUUUN
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DAATE

# technical:
set realisationnumber=$SGE_TASK_ID
set onlyOneRunBool=ZLLLL

echo
echo " **************************************"
echo " This is a clumpy simulation"
echo " **************************************"
echo 


###############################################################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_newROOT.tcsh VERITAS
echo

###############################################################################
# explanation of different directories:
# RUNDIR:  parent directory of run, the one set in line (08) of
#		   anisotropySimulation.parameters file, e.g. 14-07-13-CrabTest
# ODIR:	   parent directory of specific runparameter variation 
# WORKDIR: directory within ODIR for the number of identical realizations
# if no runparameter file: ODIR= RUNDIR
# if realizationNumber = 1: WORKDIR = ODIR

###############################################################################
# clean up output directory and create subfolders for each run:

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter

# make temporary directory for each realisation:
if ( $onlyOneRunBool == "0" ) then
	set WORKDIR=$ODIR/realisation-${realisationnumber}
	mkdir -p $WORKDIR/Inputfiles
	cd $WORKDIR
else
	set WORKDIR=$ODIR # = current directory"$(pwd)"
	cd $WORKDIR
endif

# copy transfer script and list file into Inputfiles dir:
cp $SCRIPTDIR/clumpyV2_alphaint_study.merge.py $WORKDIR/Inputfiles/merge.py

python $WORKDIR/Inputfiles/merge.py -i $ODIR/realisation-${realisationnumber}.drawn -j $ODIR/realisation-${realisationnumber}.output -o $ODIR/realisation-${realisationnumber}.drawn

rm $ODIR/realisation-${realisationnumber}.output
rm -r $WORKDIR

if ( $onlyOneRunBool == "0" ) then
	cd $ODIR
	#rm -r $WORKDIR
endif

##exit
