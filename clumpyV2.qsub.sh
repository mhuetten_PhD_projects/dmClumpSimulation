#$ -S /bin/tcsh
#
# script to perform Dark Matter clumpy simulations
#
# Author: Moritz Huetten
#
#####################################
# parameters set by parent script

set PARAMETER=RRRRR
set RUNDIR=RUUUN
set QLOG=PEEED
set ODIR=OODIR
set SCRIPTDIR=DIIIR
set DATE=DAATE

# Geometrical parameters:
set psiZeroDeg=AAAAA
set thetaZeroDegGal=BBBBB
set psiWidthDeg=CCCCC
set thetaWidthDeg=DDDDD
set alphaIntDeg=EEEEE
set beamFWHMDeg=ZNNNN

# CLUMPY technical parameters:
set user_rse=FFFFF
set seed=ZHHHH
set nside=ZMMMM

# Galactic Halo:
set gGAL_TOT_FLAG_PROFILE=HHHHH	
set gGAL_TOT_SHAPE_PARAMS_0=IIIII
set gGAL_TOT_SHAPE_PARAMS_1=JJJJJ
set gGAL_TOT_SHAPE_PARAMS_2=KKKKK
set gGAL_TOT_RSCALE=LLLLL
set gGAL_RHOSOL=MMMMM
set gGAL_RSOL=NNNNN
set gGAL_RVIR=OOOOO

# Clumps distribution:
set gGAL_DPDV_FLAG_PROFILE=PPPPP
set gGAL_DPDV_SHAPE_PARAMS_0=QQQQQ
set gGAL_DPDV_SHAPE_PARAMS_1=SSSSS
set gGAL_DPDV_SHAPE_PARAMS_2=TTTTT
set gGAL_DPDV_RSCALE=UUUUU
set gGAL_DPDM_SLOPE=VVVVV
set gDM_MMIN_SUBS=WWWWW
set gGAL_SUBS_N_INM1M2=XXXXX

# Clumps inner profile:
set gGAL_CLUMPS_FLAG_PROFILE=YYYYY
set gGAL_CLUMPS_SHAPE_PARAMS_0=ZZZZZ
set gGAL_CLUMPS_SHAPE_PARAMS_1=ZAAAA
set gGAL_CLUMPS_SHAPE_PARAMS_2=ZBBBB
set gGAL_CLUMPS_FLAG_CVIRMVIR=ZCCCC
set gDM_LOGCVIR_STDDEV=ZQQQQ

# Additional clumpy parameters:
set gDM_MMAXFRAC_SUBS=ZDDDD
set gDM_RHOSAT=ZEEEE
set gGAL_SUBS_M1=ZFFFF
set gGAL_SUBS_M2=ZGGGG

# Particle physics parameters:
set gSIMU_IS_ANNIHIL_OR_DECAY=GGGGG

# Halo list:
set gLIST_HALOES=ZIIII
set gLIST_Bool=ZJJJJ

# technical:
set realisationnumber=$SGE_TASK_ID
set onlyOneRunBool=ZLLLL
set gSIMU_N_STATISTIC_REPETITIONS=ZOOOO
set gSIMU_IS_GALPOWERSPECTRUM=ZPPPP
set gSIMU_SUBS_NUMBEROFLEVELS=ZRRRR
set gGAL_TRIAXIAL_IS=ZSSSS

set powerspectrapipeline=ZTTTT
set clumpsstatisticspipeline=ZUUUU
set integrationstudy=ZVVVV

# temporary directory
set DDIR=$TMPDIR/Moritz
mkdir -p $DDIR
rm -f $DDIR/*
echo $DDIR

echo
echo " **************************************"
echo " This is a clumpy simulation"
echo " **************************************"
echo 
echo " *** INPUT PARAMETERS: ***"
echo 
echo -n " Psi position of line of sight: " $psiZeroDeg "degrees\n"
echo -n " Theta position of line of sight: " $thetaZeroDeg "degrees\n"
echo -n " Full width of skymap around line of sight in psi direction: " $psiWidthDeg "degrees\n"
echo -n " Full width of skymap around line of sight in theta direction: " $thetaWidthDeg "degrees\n"
echo -n " grid resolution: " $nside "\n"
echo -n " integration angle: " $alphaIntDeg "degrees\n"
echo -n " user_rse: " $user_rse "\n"
echo
echo " *** FOR INFO HOW TO ADJUST THE INPUT PARAMETERS SEE dmClumps.sub.sh ***"
echo 

###############################################################################
# set the right observatory (environmental variables)
#source $EVNDISPSYS/setObservatory.tcsh VERITAS
source /afs/ifh.de/user/m/mhuetten/setbatchenvironment_newROOT.tcsh VERITAS
echo

###############################################################################
# explanation of different directories:
# RUNDIR:  parent directory of run, the one set in line (08) of
#		   anisotropySimulation.parameters file, e.g. 14-07-13-CrabTest
# ODIR:	   parent directory of specific runparameter variation 
# WORKDIR: directory within ODIR for the number of identical realizations
# if no runparameter file: ODIR= RUNDIR
# if realizationNumber = 1: WORKDIR = ODIR

###############################################################################
# clean up output directory and create subfolders for each run:

# in case of multiple runs with runparameter: RUNDIR: parent directory, ODIR: directory of specific runparameter

# make temporary directory for each realisation:
if ( $onlyOneRunBool == "0" ) then
	set WORKDIR=$ODIR/realisation-${realisationnumber}
	echo "WORKDIR: " $WORKDIR
	mkdir -p $WORKDIR/Inputfiles
	cd $WORKDIR
else
	set WORKDIR=$ODIR # = current directory"$(pwd)"
	cd $WORKDIR
endif

if ( $powerspectrapipeline == "1" ) then
	set MODE="n"
	set ISWRITEROOTFILES="0"
else
	set MODE="p"
	set ISWRITEROOTFILES="1"
endif

if ( $clumpsstatisticspipeline == "1" ) then
	set ISWRITEROOTFILES="0"
endif

###############################################################################
# modify clumpy parameter file:
echo " *** modify clumpy parameter file: ***"
sed  's|EEEEZ|'$alphaIntDeg'|' $SCRIPTDIR/clumpyV2.clumpy_params.txt > $WORKDIR/Inputfiles/tmp1.txt
sed	 's|GGGGZ|'$gSIMU_IS_ANNIHIL_OR_DECAY'|' $WORKDIR/Inputfiles/tmp1.txt > $WORKDIR/Inputfiles/tmp2.txt
sed  's|HHHHZ|'$gGAL_TOT_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp2.txt > $WORKDIR/Inputfiles/tmp3.txt
sed  's|IIIIZ|'$gGAL_TOT_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp3.txt > $WORKDIR/Inputfiles/tmp4.txt
sed  's|JJJJZ|'$gGAL_TOT_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp4.txt > $WORKDIR/Inputfiles/tmp5.txt
sed  's|KKKKZ|'$gGAL_TOT_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp5.txt > $WORKDIR/Inputfiles/tmp6.txt
sed  's|LLLLZ|'$gGAL_TOT_RSCALE'|' $WORKDIR/Inputfiles/tmp6.txt > $WORKDIR/Inputfiles/tmp7.txt
sed  's|MMMMZ|'$gGAL_RHOSOL'|' $WORKDIR/Inputfiles/tmp7.txt > $WORKDIR/Inputfiles/tmp8.txt
sed  's|NNNNZ|'$gGAL_RSOL'|' $WORKDIR/Inputfiles/tmp8.txt > $WORKDIR/Inputfiles/tmp9.txt
sed  's|OOOOZ|'$gGAL_RVIR'|' $WORKDIR/Inputfiles/tmp9.txt > $WORKDIR/Inputfiles/tmp10.txt
sed  's|PPPPZ|'$gGAL_DPDV_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp10.txt > $WORKDIR/Inputfiles/tmp11.txt
sed  's|QQQQZ|'$gGAL_DPDV_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp11.txt > $WORKDIR/Inputfiles/tmp12.txt
sed  's|SSSSZ|'$gGAL_DPDV_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp12.txt > $WORKDIR/Inputfiles/tmp13.txt
sed  's|TTTTZ|'$gGAL_DPDV_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp13.txt > $WORKDIR/Inputfiles/tmp14.txt
sed  's|UUUUZ|'$gGAL_DPDV_RSCALE'|' $WORKDIR/Inputfiles/tmp14.txt > $WORKDIR/Inputfiles/tmp15.txt
sed  's|VVVVZ|'$gGAL_DPDM_SLOPE'|' $WORKDIR/Inputfiles/tmp15.txt > $WORKDIR/Inputfiles/tmp16.txt
sed  's|WWWWZ|'$gDM_MMIN_SUBS'|' $WORKDIR/Inputfiles/tmp16.txt > $WORKDIR/Inputfiles/tmp17.txt
sed  's|XXXXZ|'$gGAL_SUBS_N_INM1M2'|' $WORKDIR/Inputfiles/tmp17.txt > $WORKDIR/Inputfiles/tmp18.txt
sed  's|YYYYZ|'$gGAL_CLUMPS_FLAG_PROFILE'|' $WORKDIR/Inputfiles/tmp18.txt > $WORKDIR/Inputfiles/tmp19.txt
sed  's|ZZZZX|'$gGAL_CLUMPS_SHAPE_PARAMS_0'|' $WORKDIR/Inputfiles/tmp19.txt > $WORKDIR/Inputfiles/tmp20.txt
sed  's|ZAAAZ|'$gGAL_CLUMPS_SHAPE_PARAMS_1'|' $WORKDIR/Inputfiles/tmp20.txt > $WORKDIR/Inputfiles/tmp21.txt
sed  's|ZBBBZ|'$gGAL_CLUMPS_SHAPE_PARAMS_2'|' $WORKDIR/Inputfiles/tmp21.txt > $WORKDIR/Inputfiles/tmp22.txt
sed  's|ZCCCZ|'$gGAL_CLUMPS_FLAG_CVIRMVIR'|' $WORKDIR/Inputfiles/tmp22.txt > $WORKDIR/Inputfiles/tmp23.txt
sed  's|ZDDDZ|'$gDM_MMAXFRAC_SUBS'|' $WORKDIR/Inputfiles/tmp23.txt > $WORKDIR/Inputfiles/tmp24.txt
sed  's|ZEEEZ|'$gDM_RHOSAT'|' $WORKDIR/Inputfiles/tmp24.txt > $WORKDIR/Inputfiles/tmp25.txt
sed  's|ZFFFZ|'$gGAL_SUBS_M1'|' $WORKDIR/Inputfiles/tmp25.txt > $WORKDIR/Inputfiles/tmp26.txt
sed  's|ZGGGZ|'$gGAL_SUBS_M2'|' $WORKDIR/Inputfiles/tmp26.txt > $WORKDIR/Inputfiles/tmp27.txt
sed  's|ZHHHZ|'$seed'|' $WORKDIR/Inputfiles/tmp27.txt > $WORKDIR/Inputfiles/tmp28.txt
sed  's|ZMMMZ|'$nside'|' $WORKDIR/Inputfiles/tmp28.txt > $WORKDIR/Inputfiles/tmp29.txt
sed  's|ZNNNZ|'$beamFWHMDeg'|' $WORKDIR/Inputfiles/tmp29.txt > $WORKDIR/Inputfiles/tmp30.txt
echo $gLIST_HALOES
sed  's|ZIIIZ|'$gLIST_HALOES'|' $WORKDIR/Inputfiles/tmp30.txt > $WORKDIR/Inputfiles/tmp31.txt
sed  's|ZOOOZ|'$gSIMU_N_STATISTIC_REPETITIONS'|' $WORKDIR/Inputfiles/tmp31.txt > $WORKDIR/Inputfiles/tmp32.txt
sed  's|ZPPPZ|'$gSIMU_IS_GALPOWERSPECTRUM'|' $WORKDIR/Inputfiles/tmp32.txt > $WORKDIR/Inputfiles/tmp33.txt
sed  's|ZQQQZ|'$gDM_LOGCVIR_STDDEV'|' $WORKDIR/Inputfiles/tmp33.txt > $WORKDIR/Inputfiles/tmp34.txt
sed  's|ZRRRZ|'$gSIMU_SUBS_NUMBEROFLEVELS'|' $WORKDIR/Inputfiles/tmp34.txt > $WORKDIR/Inputfiles/tmp35.txt
sed  's|ZTTTZ|'$ISWRITEROOTFILES'|' $WORKDIR/Inputfiles/tmp35.txt > $WORKDIR/Inputfiles/tmp36.txt
sed  's|ZSSSZ|'$gGAL_TRIAXIAL_IS'|' $WORKDIR/Inputfiles/tmp36.txt > $WORKDIR/Inputfiles/clumpyV2.clumpy_params.txt

head -n 20 $WORKDIR/Inputfiles/clumpyV2.clumpy_params.txt  
                                       
rm $WORKDIR/Inputfiles/tmp*

# copy clumpy binary into computation directory on cluster:
cp $CLUMPY/bin/clumpy $WORKDIR
if ( $clumpsstatisticspipeline == "1" ) then
	cp /afs/ifh.de/group/cta/scratch/mhuetten/Programs/clumpy_tmp/bin/clumpy $WORKDIR 
else
	cp $CLUMPY/bin/clumpy $WORKDIR
endif

# link PPPCIDM spectra
cp -r $CLUMPY/PPPC4DMID-spectra $WORKDIR

chmod u+x $WORKDIR/Inputfiles/clumpyV2.clumpy_params.txt


###############################################################################
# start clumpy routine:
echo " *** start clumpy routine: ***"
if ( $gLIST_Bool == "1" ) then
	$WORKDIR/clumpy -g${MODE}8 $WORKDIR/Inputfiles/clumpyV2.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse 0 | tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
else
	$WORKDIR/clumpy -g${MODE}7 $WORKDIR/Inputfiles/clumpyV2.clumpy_params.txt $psiZeroDeg $thetaZeroDegGal $psiWidthDeg $thetaWidthDeg $user_rse | tee $ODIR/Logfiles/clumpy-realisation-${realisationnumber}.log
endif

mv -f  -v $DDIR/* $WORKDIR/
rm -f $DDIR/*
cd $WORKDIR/
rm -f clumpy
rm -rf PPPC4DMID-spectra
if ( $onlyOneRunBool == "0" ) then
	mv Inputfiles/clumpyV2.clumpy_params.txt $ODIR/Inputfiles/clumpyV2.clumpy_params.txt
	mv $WORKDIR/Inputfiles/list-clumps.txt $WORKDIR/list-clumps.txt
	rm -rf Inputfiles
endif

if ( $powerspectrapipeline == "1" ) then
	# for power spectrum only calculation, delete everything else again
	rm -rf Logfiles
	##sleep 20
endif

if ( $clumpsstatisticspipeline == "1" ) then
	# for power spectrum only calculation, delete everything else again
	if ( $onlyOneRunBool == "0" ) then
		# check if run ended successfully:
		mv *.fits check.fits
		#if ( -f "check.fits" ) then
			if ( $integrationstudy == "0" ) then
				mv *.drawn realisation-${realisationnumber}.drawn
				cp *.drawn $ODIR/
				cd $ODIR
				rm -r $WORKDIR
			else
				mv *.list alphaint_${alphaIntDeg}.list
				rm check.fits
			endif
		#endif
	else
		rm -rf Logfiles
		rm *.fits
	endif

endif
##exit
