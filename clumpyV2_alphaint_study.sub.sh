#!/bin/sh
#
# script run clumpy simulations:
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Submission script for a clumpyV2 simulation:"
	echo
	echo "clumpyV2.sub.sh <clumpyV2.parameters> [number of simulations] [developing mode] [clumpyV2.runparameter]"
	echo
	echo "  for the clumpyV2.parameters file use template without changing line numbers"
	echo "  in case of choosing a varying parameter in a clumpyV2.runparameter file,"
	echo "  write name of parameter in first line, then al values you want to compute."
	echo "  Example:"
	echo
	echo "    user_rse"
	echo "    5.0"
	echo "    10.0"
	echo "    15.0"
	echo
	
	exit
fi

#read variables from parameter file:

# General parameters:
export runname=$(cat $1 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
export outdir=$(cat $1 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')

# CLUMPY technical parameters:
export nside=$(cat $1 	        | head -n19 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $1 		| head -n22 | tail -n1 | sed -e 's/^[ \t]*//') 
export gSIMU_SEED=$(cat $1 	    | head -n25 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $1 		| head -n31 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $1 	| head -n34 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $1 	| head -n37 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $1 	| head -n40 | tail -n1 | sed -e 's/^[ \t]*//')
export alphaIntDeg=$(cat $1 	| head -n43 | tail -n1 | sed -e 's/^[ \t]*//')
export beamFWHMDeg=$(cat $1 	| head -n46 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $1 | head -n52 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $1 	| head -n57 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $1 | head -n60 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $1 | head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $1 | head -n62 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $1 		| head -n67 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_RHOSAT=$(cat $1			 	| head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $1 			| head -n73 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $1 				| head -n76 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $1 				| head -n79 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $1 	 | head -n85 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $1 | head -n88 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $1 | head -n89 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $1 | head -n90 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $1		 | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $1   | head -n99 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $1 | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $1 | head -n103 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $1 | head -n104 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $1  | head -n108 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps mass distribution properties:
export gGAL_DPDM_SLOPE=$(cat $1			 | head -n114 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $1		 | head -n117 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $1			 | head -n120 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMAXFRAC_SUBS=$(cat $1 		 | head -n123 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M1=$(cat $1 			 | head -n126 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_M2=$(cat $1 			 | head -n129 | tail -n1 | sed -e 's/^[ \t]*//')

# Draw object from list?
export gLIST_Bool=$(cat $1		| head -n134 | tail -n1 | sed -e 's/^[ \t]*//')
export gLIST_HALOES=$(cat $1	| head -n138 | tail -n1 | sed -e 's/^[ \t]*//')

# Special parameters:
export gSIMU_N_STATISTIC_REPETITIONS=$(cat $1	| head -n144 | tail -n1 | sed -e 's/^[ \t]*//')
export gSIMU_IS_GALPOWERSPECTRUM=$(cat $1		| head -n147 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_LOGCVIR_STDDEV=$(cat $1		        | head -n150 | tail -n1 | sed -e 's/^[ \t]*//')
export gSIMU_SUBS_NUMBEROFLEVELS=$(cat $1		| head -n153 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TRIAXIAL_IS=$(cat $1		        | head -n156 | tail -n1 | sed -e 's/^[ \t]*//')

export powerspectrapipeline=$(cat $1		    | head -n159 | tail -n1 | sed -e 's/^[ \t]*//')
export clumpsstatisticspipeline=$(cat $1	    | head -n162 | tail -n1 | sed -e 's/^[ \t]*//')

#echo $runname
#echo $outdir
#echo $user_rse
#echo $alphaIntDeg
#echo $psiZeroDeg
#echo $thetaZeroDeg
#echo $psiWidthDeg
#echo $thetaWidthDeg
#echo $gSIMU_IS_ANNIHIL_OR_DECAY
#echo $gGAL_TOT_FLAG_PROFILE
#echo $gGAL_TOT_SHAPE_PARAMS_0
#echo $gGAL_TOT_SHAPE_PARAMS_1
#echo $gGAL_TOT_SHAPE_PARAMS_2
#echo $gGAL_TOT_RSCALE
#echo $gGAL_RHOSOL
#echo $gGAL_RSOL
#echo $gGAL_RVIR
#echo $gGAL_DPDV_FLAG_PROFILE
#echo $gGAL_DPDV_SHAPE_PARAMS_0
#echo $gGAL_DPDV_SHAPE_PARAMS_1
#echo $gGAL_DPDV_SHAPE_PARAMS_2
#echo $gGAL_DPDV_RSCALE
#echo $gGAL_DPDM_SLOPE
#echo $gDM_MMIN_SUBS
#echo $gGAL_SUBS_N_INM1M2
#echo $gGAL_CLUMPS_FLAG_PROFILE
#echo $gGAL_CLUMPS_SHAPE_PARAMS_0
#echo $gGAL_CLUMPS_SHAPE_PARAMS_1
#echo $gGAL_CLUMPS_SHAPE_PARAMS_2
#echo $gGAL_CLUMPS_FLAG_CVIRMVIR
#echo $gSIMU_IS_GALPOWERSPECTRUM

if [ -n "$4" ]
then
	PLIST=$4
	# number of parameters:
	export runparameter=$(cat $PLIST 	 | head -n1 | tail -n1 | sed -e 's/^[ \t]*//')
	PARAMETERS=`cat $PLIST | tail -n +2`
	echo -n "Running parameter is: "
	echo $runparameter
	echo
fi

# number of repeated realisations:
samplenumber=1
if [ -n "$2" ]; then
samplenumber=$2
fi

# Scriptdirectory is current directory, save it:
SCRIPTDIR="$(pwd)"

# Save the date:
DATE=`date +"%y%m%d"`

# Make Logfile directory for submission scripts and batch logfiles output:
cd $SCRATCH/LOGS/DMCLUMPS-SIMULATION
#mkdir -p $DATE
QLOG=$SCRATCH/LOGS/DMCLUMPS-SIMULATION/ #$DATE

BATCHLOG=$QLOG

# developing mode?
devmode=0
if [ -n "$3" ]; then
devmode=$3
fi
if [[ $devmode == "0" ]]; then
	computingTime=00:29:59
	memorySize=1G
elif [[ $devmode == "1" ]]; then
	computingTime=00:29:59
	memorySize=1G
else
	echo "EXIT: devmode variable must be either 0 or 1"
	echo
	exit
fi		

cd $outdir
mkdir -p $runname
cd $runname
RUNDIR="$(pwd)"
# now forget outdir

# skeleton script
FSCRIPT1="clumpyV2.qsub"
FSCRIPT2="clumpyV2_transfer.qsub"
FSCRIPT3="clumpyV2_alphaint_study.qsub"
FSCRIPT4="clumpyV2_merge.qsub"

if [[ $clumpsstatisticspipeline == "1" ]]; then
	mkdir -p $RUNDIR/Batchlogs
	BATCHLOG=$RUNDIR/Batchlogs
fi


###############################################################################
# If a runparameter file is parsed:
###############################################################################

if [ -n "$4" ]; then
	echo "Runparameter submission disabled."
fi

###############################################################################
# If NO runparameter file exists:
###############################################################################

if [ ! -n "$4" ]; then
	echo "now submitting single job to batch"
	# RUNDIR is equal ODIR in this case!
	
	AFIL=singlerundummy
	
	# AFIL is not used in this case!

	cd $RUNDIR
	ODIR="$(pwd)"
	#rm -r Logfiles
	#rm -r Inputfiles
		
	mkdir -p Inputfiles
	mkdir -p Logfiles
		
	cd $SCRIPTDIR
	cp clumpyV2* $ODIR/Inputfiles/
	rm $ODIR/Inputfiles/clumpyV2.clumpy_params.txt
	rm $ODIR/Inputfiles/clumpyV2.runparameter
		
	startnum=$((7926))
	one=$((1))
	endnum=$(($startnum+$samplenumber-$one))

	powerspectrapipeline_job3=$powerspectrapipeline
	powerspectrapipeline=0
	
	for i in  $(seq $startnum 1 $endnum)
	do	
		
		if [[ $samplenumber == "1" ]]; then
			FNAM1="$QLOG/clumpyV2-$runname"
			FNAM2="$QLOG/clumpyV2-transfer-$runname"
			FNAM3="$QLOG/clumpyV2-alphaintjobs-$runname"
			FNAM4="$QLOG/clumpyV2-merge-$runname"
			onlyOneRunBool=1
		else 
			FNAM1="$QLOG/clumpyV2-$runname-$i"
			FNAM2="$QLOG/clumpyV2-transfer-$runname-$i"
			FNAM3="$QLOG/clumpyV2-alphaintjobs-$runname-$i"
			FNAM4="$QLOG/clumpyV2-merge-$runname-$i"
			onlyOneRunBool=0
			echo "starting multisample realisation"
		fi
		
		integrationstudy=0	
														
		sed -e "s|FFFFF|$user_rse|" \
			-e "s|PEEED|$QLOG|" \
			-e "s|OODIR|$ODIR|" \
			-e "s|DAATE|$DATE|" \
			-e "s|MMMMM|$gGAL_RHOSOL|" \
			-e "s|NNNNN|$gGAL_RSOL|" \
			-e "s|OOOOO|$gGAL_RVIR|" \
			-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
			-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
			-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
			-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
			-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
			-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
			-e "s|WWWWW|$gDM_MMIN_SUBS|" \
			-e "s|XXXXX|$gGAL_SUBS_N_INM1M2|" \
			-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
			-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
			-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
			-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
			-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
			-e "s|AAAAA|$psiZeroDeg|" \
			-e "s|BBBBB|$thetaZeroDeg|" \
			-e "s|CCCCC|$psiWidthDeg|" \
			-e "s|DDDDD|$thetaWidthDeg|" \
			-e "s|EEEEE|$alphaIntDeg|" \
			-e "s|ZNNNN|$beamFWHMDeg|" \
			-e "s|ZMMMM|$nside|" \
			-e "s|RRRRR|$AFIL|" \
			-e "s|RUUUN|$RUNDIR|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
			-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
			-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
			-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
			-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
			-e "s|ZEEEE|$gDM_RHOSAT|" \
			-e "s|ZFFFF|$gGAL_SUBS_M1|" \
			-e "s|ZGGGG|$gGAL_SUBS_M2|" \
			-e "s|ZHHHH|$gSIMU_SEED|" \
			-e "s|ZIIII|$gLIST_HALOES|" \
			-e "s|ZJJJJ|$gLIST_Bool|" \
			-e "s|ZLLLL|$onlyOneRunBool|" \
			-e "s|ZOOOO|$gSIMU_N_STATISTIC_REPETITIONS|" \
			-e "s|ZPPPP|$gSIMU_IS_GALPOWERSPECTRUM|" \
			-e "s|ZQQQQ|$gDM_LOGCVIR_STDDEV|" \
			-e "s|ZRRRR|$gSIMU_SUBS_NUMBEROFLEVELS|" \
			-e "s|ZSSSS|$gGAL_TRIAXIAL_IS|" \
			-e "s|ZTTTT|$powerspectrapipeline|" \
			-e "s|ZUUUU|$clumpsstatisticspipeline|" \
			-e "s|ZVVVV|$integrationstudy|" \
			-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT1.sh > $FNAM1.sh
	
		chmod u+x $FNAM1.sh
		echo "script name is:" $FNAM1.sh
		
		if [[ $devmode == "1" ]]; then
			echo "submitting job in developing mode (30 minutes," $memorySize "RAM)"
		fi
	
		# submit first run which simulates the haloes:
		JOBID1=`qsub -V -terse -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_rss=1G -l tmpdir_size=1G -t $i-$i:1 -o $BATCHLOG/ -e $BATCHLOG/ -js 1 "$FNAM1.sh"` #-hold_jid 5379030
		JOBID1=${JOBID1:0:8}
		echo "Job 1: " $JOBID1
		
		# transform output *drawn file:
		
		sed -e "s|PEEED|$QLOG|" \
			-e "s|OODIR|$ODIR|" \
			-e "s|DAATE|$DATE|" \
			-e "s|RRRRR|$AFIL|" \
			-e "s|RUUUN|$RUNDIR|" \
			-e "s|ZLLLL|$onlyOneRunBool|" \
			-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT2.sh > $FNAM2.sh
		
		JOBID2=`qsub -V -terse -j y -m a -l h_cpu=00:29:59 -l h_rt=00:29:59  -l os=sl6 -l h_rss=1G -l tmpdir_size=1G -t $i-$i:1 -o $BATCHLOG/ -e $BATCHLOG/ -hold_jid $JOBID1  "$FNAM2.sh"` #
		JOBID2=${JOBID2:0:8}
		echo "Job 2: " $JOBID2
		
		# now run alpha_int variation:
		
		integrationstudy_job2=1
		gGAL_SUBS_N_INM1M2_job2=0
		gLIST_Bool_job2=1
		onlyOneRunBool_job3=0
		clumpsstatisticspipeline_job3=1
		
		if [[ $onlyOneRunBool == "0" ]]; then
			WORKDIR=$ODIR/realisation-${i}
		else
			WORKDIR=$ODIR # = current directory"$(pwd)"
		fi
		
		gLIST_HALOES_job2=$WORKDIR/Inputfiles/list-clumps.txt
														
		sed -e "s|FFFFF|$user_rse|" \
			-e "s|PEEED|$QLOG|" \
			-e "s|OODIR|$ODIR|" \
			-e "s|DAATE|$DATE|" \
			-e "s|MMMMM|$gGAL_RHOSOL|" \
			-e "s|NNNNN|$gGAL_RSOL|" \
			-e "s|OOOOO|$gGAL_RVIR|" \
			-e "s|PPPPP|$gGAL_DPDV_FLAG_PROFILE|" \
			-e "s|QQQQQ|$gGAL_DPDV_SHAPE_PARAMS_0|" \
			-e "s|SSSSS|$gGAL_DPDV_SHAPE_PARAMS_1|" \
			-e "s|TTTTT|$gGAL_DPDV_SHAPE_PARAMS_2|" \
			-e "s|UUUUU|$gGAL_DPDV_RSCALE|" \
			-e "s|VVVVV|$gGAL_DPDM_SLOPE|" \
			-e "s|WWWWW|$gDM_MMIN_SUBS|" \
			-e "s|XXXXX|$gGAL_SUBS_N_INM1M2_job2|" \
			-e "s|YYYYY|$gGAL_CLUMPS_FLAG_PROFILE|" \
			-e "s|ZZZZZ|$gGAL_CLUMPS_SHAPE_PARAMS_0|" \
			-e "s|ZAAAA|$gGAL_CLUMPS_SHAPE_PARAMS_1|" \
			-e "s|ZBBBB|$gGAL_CLUMPS_SHAPE_PARAMS_2|" \
			-e "s|ZCCCC|$gGAL_CLUMPS_FLAG_CVIRMVIR|" \
			-e "s|AAAAA|$psiZeroDeg|" \
			-e "s|BBBBB|$thetaZeroDeg|" \
			-e "s|CCCCC|$psiWidthDeg|" \
			-e "s|DDDDD|$thetaWidthDeg|" \
			-e "s|EEEEE|$alphaIntDeg|" \
			-e "s|ZNNNN|$beamFWHMDeg|" \
			-e "s|ZMMMM|$nside|" \
			-e "s|RRRRR|$AFIL|" \
			-e "s|RUUUN|$WORKDIR|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|GGGGG|$gSIMU_IS_ANNIHIL_OR_DECAY|" \
			-e "s|HHHHH|$gGAL_TOT_FLAG_PROFILE|" \
			-e "s|IIIII|$gGAL_TOT_SHAPE_PARAMS_0|" \
			-e "s|JJJJJ|$gGAL_TOT_SHAPE_PARAMS_1|" \
			-e "s|KKKKK|$gGAL_TOT_SHAPE_PARAMS_2|" \
			-e "s|LLLLL|$gGAL_TOT_RSCALE|" \
			-e "s|ZDDDD|$gDM_MMAXFRAC_SUBS|" \
			-e "s|ZEEEE|$gDM_RHOSAT|" \
			-e "s|ZFFFF|$gGAL_SUBS_M1|" \
			-e "s|ZGGGG|$gGAL_SUBS_M2|" \
			-e "s|ZHHHH|$gSIMU_SEED|" \
			-e "s|ZIIII|$gLIST_HALOES_job2|" \
			-e "s|ZJJJJ|$gLIST_Bool_job2|" \
			-e "s|ZLLLL|$onlyOneRunBool_job3|" \
			-e "s|ZOOOO|$gSIMU_N_STATISTIC_REPETITIONS|" \
			-e "s|ZPPPP|$gSIMU_IS_GALPOWERSPECTRUM|" \
			-e "s|ZQQQQ|$gDM_LOGCVIR_STDDEV|" \
			-e "s|ZRRRR|$gSIMU_SUBS_NUMBEROFLEVELS|" \
			-e "s|ZSSSS|$gGAL_TRIAXIAL_IS|" \
			-e "s|ZTTTT|$powerspectrapipeline_job3|" \
			-e "s|ZUUUU|$clumpsstatisticspipeline_job3|" \
			-e "s|ZVVVV|$integrationstudy_job2|" \
			-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT3.sh > $FNAM3.sh
			
		JOBID3=`qsub -V -terse -j y -m a -l h_cpu=$computingTime -l h_rt=$computingTime  -l os=sl6 -l h_rss=$memorySize -l tmpdir_size=$memorySize -t $i-$i:1 -o $BATCHLOG/ -e $BATCHLOG/ -hold_jid $JOBID2  -js 100000000000000 "$FNAM3.sh"` 
		JOBID3=${JOBID3:0:8}
		echo "Job 3: " $JOBID3
		
		# merge data:
		
		sed -e "s|PEEED|$QLOG|" \
			-e "s|OODIR|$ODIR|" \
			-e "s|DAATE|$DATE|" \
			-e "s|RRRRR|$AFIL|" \
			-e "s|RUUUN|$RUNDIR|" \
			-e "s|ZLLLL|$onlyOneRunBool|" \
			-e "s|DIIIR|$SCRIPTDIR|"  $FSCRIPT4.sh > $FNAM4.sh
		
		JOBID4=`qsub -V -terse -j y -m a -l h_cpu=00:29:59 -l h_rt=00:29:59  -l os=sl6 -l h_rss=1G -l tmpdir_size=1G -t $i-$i:1 -o $BATCHLOG/ -e $BATCHLOG/ -hold_jid $JOBID3  "$FNAM4.sh"` #
		JOBID4=${JOBID4:0:8}
		echo "Job 4: " $JOBID4
		
		
	done
fi
exit
