#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

###############################################################################
# Python script for evaluating the Population studies for ICRC 2015

###############################################################################
##    import modules:    ######################################################
###############################################################################

from matplotlib import pyplot as plt
import pylab
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
import numpy as np
from matplotlib import rc,rcParams
from scipy import stats
import os, glob
import bisect
from pylab import text

plotRatio = True

pdf_name = 'PopStudy-all.pdf'
png_name = 'PopStudy-all.png'


#################


# alpha_int = 0.1 deg
indir1 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-0.1deg-1e2"
indir2 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-0.1deg-1e5"

# alpha_int = 0.12 deg
#indir1 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-06-05-Population-Study"
#indir2 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-06-16-Population-Study"
 
os.chdir(indir1)

J_transition        = 18.5
J_limit_lower       = 15
J_limit_upper       = 21
if plotRatio:
    J_limit_lower_small = 16.5
    J_limit_upper_small = 20
else:
    J_limit_lower_small = J_limit_lower
    J_limit_upper_small = J_limit_upper
    
#COLOR = ['blueviolet','purple','darkblue','blue','darkturquoise','darkseagreen','green','chartreuse','y','orange','goldenrod','red','darkred','brown','darkgray' ,'black','pink']
COLOR = ['blue','darkgreen','red','darkturquoise','blue','darkgreen','red','darkturquoise','purple','darkgray','orange','chartreuse','purple','darkgray','orange','chartreuse']

line_style = ['-','-','-','-','--','--','--','--','-','-','-','-','--','--','--','--',]

#j_fit_lower = [16, 16, 16.5, 16.5, 16.5, 16, 16.5, 16.5, 16.5, 16.5, 16.5, 17,   16.5, 16.5, 16.5, 16.5]
#j_fit_upper = [17, 17, 17,   17,   17,   17, 17,   17,   17,   17.5, 17.5, 17.5, 17.5, 17.5, 17.5, 17]

j_fit_lower = [16.5, 16.5, 16.5, 16.5, 16.5, 16, 16.5, 16.5, 16.5, 16.5, 16.5, 16.5,   16.5, 16.5, 16.5, 16.5]
j_fit_upper = [17.5,   17.5,   17.5,   17.5,   17.5,   17.5, 17.5,   17.5,   17.5,  17.5,   17.5,   17.5, 17.5, 17.5, 17.5, 17.5]
j_fit_upper = [17, 17, 17,   17,   17,   17, 17,   17,   17,   17, 17, 17, 17, 17, 17, 17]



fontsize        = 19
inline_fontsize = 17
line_width      = 2

fig1 = plt.figure(figsize=(13,7))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
ax1 = fig1.add_subplot(111)
plt.grid()


# fig7 = plt.figure(figsize=(11,7))
# plt.rc('font', family='serif')
# plt.rc('text',usetex=True)
# plt.rcParams.update({'font.size': fontsize})
# ax8 = fig7.add_subplot(111)
# plt.grid()

data_ref = np.zeros((1,1))
data_ref[0,0] = 1e5

for runnumber in range(16):#[2,9]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        if min(data1[:,0]) < min(data_ref[:,0]):
            data_ref = data1
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)
    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction
    
   
    runlabel = r"model "+str(runnumber+1)
    #ax1.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=2., color=COLOR, label=runlabel)

    line_width_run = line_width

    if runnumber == 8:
        line_width_run = 1.5 * line_width
        #line_style[runnumber] = '--'

    ax1.plot(data1[:,0],data1[:,3],linestyle=line_style[runnumber], linewidth=line_width_run, color=COLOR[runnumber], label=runlabel)
    itemindex_transition_lower = bisect.bisect(data1[:,0], j_fit_lower[runnumber])
    itemindex_transition_upper = bisect.bisect(data1[:,0], j_fit_upper[runnumber])
    print  itemindex_transition_lower,  itemindex_transition_upper
    ## fit:
    for i in range(len(data1[:,1])):
        logdelta = data1[1,0] - data1[0,0]
        delta = 10**(data1[i,0] + 0.5 * logdelta ) - 10**(data1[i,0] - 0.5 * logdelta) 
        data1[i,1] = data1[i,1]/delta
         
    #ax8.plot(data1[:,0],data1[:,1],linestyle=line_style, linewidth=line_width_run, color=COLOR[runnumber], label=runlabel)
         
    slope, intercept, r_value, p_value, std_err = stats.linregress(data1[itemindex_transition_lower:itemindex_transition_upper,0],np.log10(data1[itemindex_transition_lower:itemindex_transition_upper,1]))
    print "Run",runnumber+1," differential slope:", slope, " error on slope", std_err
    slope, intercept, r_value, p_value, std_err = stats.linregress(data1[itemindex_transition_lower:itemindex_transition_upper,0],np.log10(data1[itemindex_transition_lower:itemindex_transition_upper,3]))
    print "Run",runnumber+1," integrated slope:", slope, " error on slope", std_err
 
    print ""

#pylab.ylim([1e-2,1e4])
ax1.set_ylim((1e-2,1e4))

box = ax1.get_position()
ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height])


leg1 = ax1.legend(loc='center left',prop={'size':inline_fontsize}) #, bbox_to_anchor=(1, 0.5)
leg1.get_frame().set_alpha(0.8)
ax1.set_ylabel(r'$N_{sub}(\geq J)$');
ax1.set_xlabel(r'Astrophysical $J$-factor in $log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$');


ax1.axvline(x=18.23, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
ax1.axvspan(18.18, 18.29, alpha=0.5, color='grey')
fig1.text(0.45, 0.11,'Sculptor dSphG', ha='center', va='bottom', transform=ax1.transAxes, rotation = 'vertical', size = inline_fontsize)


#ax8.legend(loc='lower left',prop={'size':inline_fontsize})
#ax8.set_ylabel(r'$dN_{sub}/dJ$');
#ax8.set_xlabel(r'Astrophysical $J$-factor in $log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$');



ax1.set_yscale('log')
#ax8.set_yscale('log')



itemindex_lower = bisect.bisect(data_ref[:,0], J_limit_lower)
itemindex_upper = bisect.bisect(data_ref[:,0], J_limit_upper)


#print data_ref[:,8]
#print data_ref[:,0]
#print len(data1[:,8])

def tick_function(X):
    print X
    V = np.zeros(len(X))
    for i in range(len(X)):
        itemindex = bisect.bisect(data1[:,0], X[i])
        if itemindex > len(X):
            itemindex -= 1
        
        V[i] = data1[itemindex,8]
        #print itemindex, data1[itemindex,8]
    return [r"$%.1e$" % z for z in V]

#print data_ref[itemindex_lower,8], data_ref[itemindex_upper,8]

data_ref[:,8] = 100. * data_ref[:,8]

ax2 = ax1.twiny()
ax2.plot(data_ref[:,8],data_ref[:,3],alpha=0)
ax2.set_xscale('log')
ax2.set_xlim((data_ref[itemindex_lower,8],data_ref[itemindex_upper,8]))

pylab.ylim([1e-2,1e4])

#ax1.axvline(x=11.96, ymin=0., ymax=1., color='grey')
#ax1.axvspan(11.55, 12.33, alpha=0.5, color='grey')
#text(0.37, 0.22,'Coma Berenices dwarf', ha='center', va='center', transform=ax1.transAxes, rotation = 'vertical', size = 14)



#ax1.axvline(x=12.40, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
#ax1.axvspan(12.05, 12.72, alpha=0.5, color='grey')
#text(0.47, 0.05,'Segue I dSphG', ha='center', va='bottom', transform=ax1.transAxes, rotation = 'vertical', size = 14)

ax1.axhline(y=1, xmin=0, xmax=1, color='k', linestyle='-', linewidth=1.5)
#fig1.text(0.735, 0.7,'CTA survey sensitivity', ha='center', va='center', transform=ax1.transAxes, rotation = 'vertical', size = inline_fontsize)


ax1.set_xlim((J_limit_lower,J_limit_upper))

#ax2.set_xticks(ax1.get_xticks())
#ax2.set_xbound(ax1.get_xbound())
#ax2.set_xticklabels(tick_function(ax1.get_xticks()))


ax2.set_xlabel(r'flux $>100\,\mathrm{GeV}$ in  Crab  units  for  $\chi\rightarrow b\bar{b},\,\langle\sigma v\rangle = 3\cdot 10^{-24}\, \mathrm{cm^3 s^{-1}},\,m_{\chi}=500\,\mathrm{GeV}$');



os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")

fig1.savefig(pdf_name, format='pdf', dpi=300)
fig1.savefig(png_name, format='png', dpi=300)

###############################################################################
# reference model:

for runnumber in [8]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        data_ref = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        data_ref2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data_ref[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data_ref2[:,0], J_transition)

    correction = data_ref[itemindex_transition1,3] / data_ref2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data_ref[i,0] >= J_transition:
            J_i = data_ref[i,0]
            itemindex = bisect.bisect(data_ref2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data_ref2):
                data_ref[i,3] = data_ref2[itemindex-1,3] * correction


###############################################################################

fig7 = plt.figure(figsize=(7.1,4.06))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)
plt.grid()
ax8 = fig7.add_subplot(111)



fig2 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax3 = fig2.add_subplot(111)





for runnumber in [0,8]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)

    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction
    
    sigma_c = [0.14,0,0,0,0,0,0,0,0.24]
    runlabel = r"model "+str(runnumber+1)+": $\sigma_c=$ "+str(sigma_c[runnumber])
    #ax1.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=2., color=COLOR, label=runlabel)
    if plotRatio:
        ax3.plot(data1[:,0],data1[:,3]/data_ref[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)
        if runnumber == 8:
            pass
            #ax8.plot(data1[:,0],data1[:,3]/data_ref[:,3], linewidth=line_width)
        else:
            ax8.plot(data1[:,0],data1[:,3]/data_ref[:,3], linewidth=line_width, label=runlabel)
    else:
        ax3.plot(data1[:,0],data1[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)


    print ""



if plotRatio:
    ax3.legend(loc='upper left',prop={'size':inline_fontsize})
    ax3.set_ylabel(r'$N_{sub}/N_{sub}^{\rm reference}$')
    ax3.axvline(x=19.2, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
else:
    ax3.legend(loc='lower left',prop={'size':inline_fontsize})
    ax3.set_ylabel(r'$N_{sub}(\geq J)$');
ax3.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$')

plt.grid()
#plt.xscale('lin')

if not plotRatio:
    ax3.set_yscale('log')

ax3.set_xlim((J_limit_lower_small,J_limit_upper_small))

if plotRatio:
    pylab.ylim([0.,5.])
else:
    pylab.ylim([1e-2,1e4])

os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-cvirscatter.pdf'
plt.savefig(pdf_name, format='pdf', dpi=300)

###############################################################################

fig5 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax6 = fig5.add_subplot(111)

for runnumber in [8,9]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)
    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction
    
    dpdv = [0,0,0,0,0,0,0,'EINASTO','MADAU','EINASTO']
    runlabel1 = r"model "+str(runnumber+1)
    runlabel2 = r" $\varrho_{\rm sub}$ "+dpdv[runnumber]
    runlabel = runlabel1 + runlabel2
    #ax1.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=2., color=COLOR, label=runlabel)
    if plotRatio:
        ax6.plot(data1[:,0],data1[:,3] / data_ref[:,3] ,linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)
        if runnumber != 8:
            ax8.plot(data1[:,0],data1[:,3] / data_ref[:,3], linewidth=line_width, label=runlabel)
    else:
        ax6.plot(data1[:,0],data1[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)


    print ""


if plotRatio:
    ax6.legend(loc='upper left',prop={'size':inline_fontsize})
else:
    ax6.legend(loc='lower left',prop={'size':inline_fontsize})

if plotRatio:
    ax6.axvline(x=19.2, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
    ax6.set_ylabel(r'$N_{sub}/N_{sub}^{\rm reference}$')
else:
    ax6.set_ylabel(r'$N_{sub}(\geq J)$');
ax6.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$')

plt.grid()
#plt.xscale('lin')

if not plotRatio:
    ax6.set_yscale('log')

ax6.set_xlim((J_limit_lower_small,J_limit_upper_small))

if plotRatio:
    pylab.ylim([0.,5.])
    a=5
else:
    pylab.ylim([1e-2,1e4])
#pylab.ylim([1e-2,1e4])

os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-dpdv.pdf'
plt.savefig(pdf_name, format='pdf', dpi=300)



###############################################################################


fig4 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax5 = fig4.add_subplot(111)

for runnumber in [8,10]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)
    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction

    dpdm = [0,0,0,0,0,0,0,0,1.9,0,2.0,0,1.9,0,2.0]
    #runlabel = r'model '+str(runnumber+1)+': $  \alpha \gamma_{dN/dM}=$ '+str(dpdm[runnumber])
    runlabel0 = r'model '+str(runnumber+1)+': '
    runlabel1 = r'$\alpha_m =$ '+str(dpdm[runnumber])
    runlabel = runlabel0 + runlabel1
    #ax1.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=2., color=COLOR, label=runlabel)
    if plotRatio:
        ax5.plot(data1[:,0],data1[:,3]/data_ref[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)
        if runnumber != 8:
            ax8.plot(data1[:,0],data1[:,3]/data_ref[:,3], linewidth=line_width, label=runlabel)
    else:
        ax5.plot(data1[:,0],data1[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)


    print ""


if plotRatio:
    ax5.legend(loc='upper right',prop={'size':inline_fontsize})
else:
    ax5.legend(loc='lower left',prop={'size':inline_fontsize})

if plotRatio:
    ax5.axvline(x=19.2, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
    ax5.set_ylabel(r'$N_{sub}/N_{sub}^{\rm reference}$')
else:
    ax5.set_ylabel(r'$N_{sub}(\geq J)$');
ax5.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$')

plt.grid()
#plt.xscale('lin')

if not plotRatio:
    ax5.set_yscale('log')

ax5.set_xlim((J_limit_lower_small,J_limit_upper_small))

if plotRatio:
    pylab.ylim([0.,5.])
else:
    pylab.ylim([1e-2,1e4])

#pylab.ylim([1e-2,1e4])

os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-dpdm.pdf'
plt.savefig(pdf_name, format='pdf', dpi=300)


###############################################################################



fig3 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax4 = fig3.add_subplot(111)

for runnumber in [8,12]:
#for runname in glob.glob('Run*'):
    runname = "Run"+str(runnumber+1)
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
 
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)
    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
 
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction
    
    cvir = [0,0,0,0,0,0,0,0,'SANCHEZ',0,0,0,'BULLOCK']
    runlabel = r"model "+str(runnumber+1)+": $c({M_{sub}})$ "+cvir[runnumber]
    #ax1.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=2., color=COLOR, label=runlabel)
    if plotRatio:
        ax4.plot(data1[:,0],data1[:,3]/data_ref[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)
        if runnumber != 8:
            cvir = [0,0,0,0,0,0,0,0,'SAN.',0,0,0,'BULLOCK']
            runlabel = r"model "+str(runnumber+1)+": $c({M_{sub}})$ "+cvir[runnumber]
            ax8.plot(data1[:,0],data1[:,3]/data_ref[:,3],linewidth=line_width, label=runlabel)
    else:
        ax4.plot(data1[:,0],data1[:,3],linestyle=line_style[runnumber], linewidth=line_width, color=COLOR[runnumber], label=runlabel)


    print ""

if plotRatio:
    ax4.legend(loc='upper left',prop={'size':inline_fontsize})
else:
    ax4.legend(loc='lower left',prop={'size':inline_fontsize})

if plotRatio:
    ax4.axvline(x=19.2, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
    ax4.set_ylabel(r'$N_{sub}/N_{sub}^{\rm reference}$')
else:
    ax4.set_ylabel(r'$N_{sub}(\geq J)$');
ax4.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$')

plt.grid()
#plt.xscale('lin')

if not plotRatio:
    ax4.set_yscale('log')

ax4.set_xlim((J_limit_lower_small,J_limit_upper_small))

if plotRatio:
    pylab.ylim([0.,5.])
else:
    pylab.ylim([1e-2,1e4])

os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-cvir.pdf'
plt.savefig(pdf_name, format='pdf', dpi=300)


###############################################################################

ax8.set_ylim((0,6.))
leg8 = ax8.legend(loc='upper left',prop={'size':inline_fontsize})
leg8.get_frame().set_alpha(0.8)


ax8.axvline(x=19.2, ymin=0., ymax=1., color='grey', linestyle='--', linewidth=1.5)
ax8.set_ylabel(r'$N_{sub}/N_{sub}^{\rm reference}$')

ax8.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$')

ax8.set_xlim((J_limit_lower_small,J_limit_upper_small))




os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-ratio-all.pdf'
png_name = 'PopStudy-ratio-all.png'
fig7.savefig(pdf_name, format='pdf', dpi=300)
fig7.savefig(png_name, format='png', dpi=300)

###############################################################################



fig6 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax7 = fig6.add_subplot(111)

data_ref = np.zeros((1,1))
data_ref[0,0] = 1e5

indir1 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-1FOV-0.1deg-1e2"
indir2 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-1FOV-0.1deg-1e5"


J_transition = 17

color_i = 0

for runnumber in [30,60, 90, 120, 150 ]:
#for runname in glob.glob('Run*'):
    runname = str(runnumber)+"deg"
    print "current run to be evaluated:", runname
    rundir1 = indir1+"/"+runname
    rundir2 = indir2+"/"+runname
    
    os.chdir(rundir1)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data1 = np.loadtxt(rundir1+"/"+filename)#, comments="#", names=True)
        
    os.chdir(rundir2)
    for filename in glob.glob('*.stat'):
        print " file to be read:", filename
        data2 = np.loadtxt(rundir2+"/"+filename)#, comments="#", names=True)
   
    itemindex_transition1 = bisect.bisect(data1[:,0], J_transition)
    itemindex_transition2 = bisect.bisect(data2[:,0], J_transition)
    correction = data1[itemindex_transition1,3] / data2[itemindex_transition2,3]
    
    for i in range(len(data1)):
        if data1[i,0] >= J_transition:
            J_i = data1[i,0]
            itemindex = bisect.bisect(data2[:,0], J_i)
            #print i, itemindex
            if itemindex < len(data2):
                data1[i,3] = data2[itemindex-1,3] * correction
 
         
 
    runlabel = r"$\Delta b =$"+str(runnumber)+"$^{\circ}$"
    ax7.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=line_width,  label=runlabel)
    color_i += 1
 
    print ""


ax7.legend(loc='upper right',prop={'size':inline_fontsize})
ax7.set_ylabel(r'$N_{sub}(\geq J)$');
ax7.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$');

plt.grid()
#plt.xscale('lin')

# smooth background
ax7.axvline(x=17.53, ymin=0., ymax=1., color=COLOR[0], linestyle='--', linewidth=1.5)
ax7.axvline(x=16.86, ymin=0., ymax=1., color=COLOR[1], linestyle='--', linewidth=1.5)
ax7.axvline(x=16.49, ymin=0., ymax=1., color=COLOR[2], linestyle='--', linewidth=1.5)
ax7.axvline(x=16.27, ymin=0., ymax=1., color=COLOR[3], linestyle='--', linewidth=1.5)
ax7.axvline(x=16.16, ymin=0., ymax=1., color=COLOR[8], linestyle='--', linewidth=1.5)


ax7.set_yscale('log')
ax7.set_xlim((J_limit_lower,J_limit_upper))
ax7.axhline(y=1, xmin=0, xmax=1, color='k', linestyle='-', linewidth=1.5)

pylab.ylim([1e-2,1e4])

os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")
pdf_name = 'PopStudy-1FOV.pdf'
png_name = 'PopStudy-1FOV.png'
plt.savefig(pdf_name, format='pdf', dpi=300)
plt.savefig(png_name, format='png', dpi=300)

###############################################################################



fig8 = plt.figure(figsize=(7,4))
plt.rc('font', family='serif')
plt.rc('text',usetex=True)
plt.rcParams.update({'font.size': fontsize})
plt.gcf().subplots_adjust(bottom=0.15)

ax9 = fig8.add_subplot(111)

data_ref = np.zeros((1,1))
data_ref[0,0] = 1e5

indir1 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-06-16-Population-Study/Run9"
indir2 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-0.2deg/Run9"
indir3 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-0.1deg-1e5/Run9"
indir4 = "/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-07-01-Population-Study-0.05deg/Run9"

os.chdir(indir1)
for filename in glob.glob('*.stat'):
    print " file to be read:", filename
    data1 = np.loadtxt(indir1+"/"+filename)#, comments="#", names=True)
    
os.chdir(indir2)
for filename in glob.glob('*.stat'):
    print " file to be read:", filename
    data2 = np.loadtxt(indir2+"/"+filename)#, comments="#", names=True)
   
os.chdir(indir3)
for filename in glob.glob('*.stat'):
    print " file to be read:", filename
    data3 = np.loadtxt(indir3+"/"+filename)#, comments="#", names=True)
 
os.chdir(indir4)
for filename in glob.glob('*.stat'):
    print " file to be read:", filename
    data4 = np.loadtxt(indir4+"/"+filename)#, comments="#", names=True)         
 
runlabel1 = r"$\theta = 0.12 deg$"
ax9.plot(data1[:,0],data1[:,3],linestyle='-', linewidth=line_width,  label=runlabel1)
runlabel2 = r"$\theta = 0.2 deg$"
ax9.plot(data2[:,0],data2[:,3],linestyle='-', linewidth=line_width,  label=runlabel2)
runlabel3 = r"$\theta = 0.1 deg$"
ax9.plot(data3[:,0],data3[:,3],linestyle='-', linewidth=line_width,  label=runlabel3)
runlabel4 = r"$\theta = 0.05 deg$"
ax9.plot(data4[:,0],data4[:,3],linestyle='-', linewidth=line_width,  label=runlabel4)



ax9.legend(loc='upper right',prop={'size':inline_fontsize})
ax9.set_ylabel(r'$N_{sub}(\geq J)$');
ax9.set_xlabel(r'$log_{10}(J/\mathrm{GeV^2\,cm^{-5}})$');

plt.grid()
#plt.xscale('lin')

ax9.set_yscale('log')
ax9.set_xlim((J_limit_lower,J_limit_upper))

pylab.ylim([1e-2,1e4])




plt.show()

