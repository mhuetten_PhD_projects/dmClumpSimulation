# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os, glob
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
from matplotlib import rc,rcParams

rc('text',usetex=True)

def main(argv):
    
    ###########################################################################
    # read input variables

    indir2 = "empty"
    lmax_plot = 500
    try:
        opts, args = getopt.getopt(sys.argv[1:],"i:",["infile="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print 'xxx'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'The input options are:'
            print 'xxx'
            sys.exit()
        elif opt in ("-i", "--infile"):
            infile = str(arg)
    
    print " initializing script clumpyV2.plotFlocal.py"

    data1 = np.loadtxt(infile)
    data2 = np.loadtxt('output/gal.rhor-kGAO.output')
    data3 = np.loadtxt('output/gal.rhor-kEINASTO.output')
    data4 = np.loadtxt('output/gal.rhor-kAQUARIUS_SUB.output')
   
    plot_flocal = plt.figure(figsize=(12, 8)) 


    #p1 = plt.plot(data2[:,0], data1[:,4],color='blue',  linestyle='-', linewidth=1.5, label=r"input")
    p2 = plt.plot(data1[:,0], data2[:,4],color='green',  linestyle='-', linewidth=1.5, label=r"Madau (2008)")
    p3 = plt.plot(data3[:,0], data3[:,4],color='red',  linestyle='-', linewidth=1.5, label=r"Einasto $\alpha_E=0.68$, $r_s=199\,kpc$")
    p4 = plt.plot(data3[:,0], data4[:,4],color='black',  linestyle='-', linewidth=1.5, label=r"Aquarius $f_{sub}$ (Springel 2008)")
    
    title = r'$f_{\rm local}$ for different subhalo distributions in the reference halo'
    plt.title('\n'.join(wrap(title,100)), fontsize=16)
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='lower right',prop={'size':14})

    plt.xlabel(r"$r\;[kpc]$"); plt.ylabel(r'$f_{\rm local} = \frac{\langle \rho_{\rm sub}\rangle}{\rho_{\rm tot}}$'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1e-1,500])
    pylab.ylim([1e-5,1])
    rcParams.update({'font.size': 16})

    plot_nsubs = plt.figure(figsize=(12, 8)) 

    #p1 = plt.plot(data2[:,0], data2[:,5],color='blue',  linestyle='-', linewidth=1.5, label=r"Einasto antibiased")
    p2 = plt.plot(data1[:,0], data1[:,5],color='green',  linestyle='-', linewidth=1.5, label=r"Madau (2008)")
    p3 = plt.plot(data3[:,0], data3[:,5],color='red',  linestyle='-', linewidth=1.5, label=r"Einasto $\alpha_E=0.68$, $r_s=199\,kpc$")
    p4 = plt.plot(data3[:,0], data4[:,5],color='black',  linestyle='-', linewidth=1.5, label=r"Aquarius $f_{sub}$ (Springel 2008)")
    
    title = r'Amount of clumps $N(< r)$ for different subhalo distributions in the reference halo'
    plt.title('\n'.join(wrap(title,100)), fontsize=16)
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='lower right',prop={'size':14})

    plt.xlabel(r"$r\;[kpc]$"); plt.ylabel(r'$N(< r) / N_{\rm tot}$'); plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1e-1,500])
    pylab.ylim([1e-5,1])
    rcParams.update({'font.size': 16})    
    
    plt.show()
    
    print "finished."
    print ""
    
if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################    

