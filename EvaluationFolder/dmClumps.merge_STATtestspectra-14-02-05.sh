#!/bin/sh
#
# script run toy MonteCarlo for Anisotropies
#
# Author: Moritz Huetten
# 

if [ ! -n "$1" ]
then
	echo
	echo "Generating skymap script for clumpy Simulation:"
	echo
	echo "dmClumps.sub.sh <output directory>  [plot or fits  [ [Jtot or Jclumps] [plotview]  [lower exponent] [upper exponent] ]" 
	echo
	echo "  - if no optional arguments are given, a data file will be written and Jtot will be plotted in cartesian projection. "
	echo "  - if the option fits is given, all further arguments won't be considered (and can also be omitted)"
	echo "  - if the option plot is given with no further arguments, Jtot is plotted in cartesian projection"
	echo "  plotview is either 'cart' (cartesian projection) or 'moll' (mollweide) projection "
	echo "  lower/upper exponent: plotting range for logarithmic plot "
	echo "  command 'plot' plots only skymap (but of course only if .healpix file has been already created before), 'fits' creates only .healpix file without plotting."
	echo 

   exit
fi

outdir=$1





# Scriptdirectory is current directory:
SCRIPTDIR="$(pwd)"

#############################################
#read variables from parameter files:

# This is hardcoded for convenience (for not needing to input each time the full path ouf the outputdirectory)

SCRIPTDIR=/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/EvaluationFolder
WORKDIR=/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation

parameterfile=${WORKDIR}/${outdir}/Inputfiles/dmClumps.parameters
#PLIST=${WORKDIR}/${outdir}/Inputfiles/dmClumps.runparameter

#read variables from parameter file:

# General parameters:
#export runname=$(cat $parameterfile 		| head -n9 | tail -n1 | sed -e 's/^[ \t]*//')
#export outdir=$(cat $parameterfile 			| head -n12 | tail -n1 | sed -e 's/^[ \t]*//')
export user_rse=$(cat $parameterfile 		| head -n15 | tail -n1 | sed -e 's/^[ \t]*//') # not used from here, but from runparamter file!
export alphaIntDeg=$(cat $parameterfile 	| head -n18 | tail -n1 | sed -e 's/^[ \t]*//')

# Geometrical parameters:
export psiZeroDeg=$(cat $parameterfile 		| head -n24 | tail -n1 | sed -e 's/^[ \t]*//') 
export thetaZeroDeg=$(cat $parameterfile 	| head -n27 | tail -n1 | sed -e 's/^[ \t]*//')
export psiWidthDeg=$(cat $parameterfile 	| head -n30 | tail -n1 | sed -e 's/^[ \t]*//')
export thetaWidthDeg=$(cat $parameterfile 	| head -n33 | tail -n1 | sed -e 's/^[ \t]*//')

# Physics parameters:
export gSIMU_IS_ANNIHIL_OR_DECAY=$(cat $parameterfile | head -n39 | tail -n1 | sed -e 's/^[ \t]*//')

# Galactic Halo:
export gGAL_TOT_FLAG_PROFILE=$(cat $parameterfile 	| head -n45 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_0=$(cat $parameterfile | head -n48 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_1=$(cat $parameterfile | head -n49 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_SHAPE_PARAMS_2=$(cat $parameterfile | head -n50 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_TOT_RSCALE=$(cat $parameterfile 		| head -n55 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RHOSOL=$(cat $parameterfile 			| head -n58 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RSOL=$(cat $parameterfile 				| head -n61 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_RVIR=$(cat $parameterfile 				| head -n64 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps distribution:
export gGAL_DPDV_FLAG_PROFILE=$(cat $parameterfile 	 | head -n70 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_0=$(cat $parameterfile | head -n73 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_1=$(cat $parameterfile | head -n74 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_SHAPE_PARAMS_2=$(cat $parameterfile | head -n75 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDV_RSCALE=$(cat $parameterfile		 | head -n78 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_DPDM_SLOPE=$(cat $parameterfile			 | head -n81 | tail -n1 | sed -e 's/^[ \t]*//')
export gDM_MMIN_SUBS=$(cat $parameterfile			 | head -n84 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_SUBS_N_INM1M2=$(cat $parameterfile		 | head -n87 | tail -n1 | sed -e 's/^[ \t]*//')

# Clumps inner profile:
export gGAL_CLUMPS_FLAG_PROFILE=$(cat $parameterfile   | head -n93 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_0=$(cat $parameterfile | head -n96 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_1=$(cat $parameterfile | head -n97 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_SHAPE_PARAMS_2=$(cat $parameterfile | head -n98 | tail -n1 | sed -e 's/^[ \t]*//')
export gGAL_CLUMPS_FLAG_CVIRMVIR=$(cat $parameterfile  | head -n102 | tail -n1 | sed -e 's/^[ \t]*//')



###############################################################################################################
# number of parameters:
#PARAMETERS=`cat $PLIST`



#
#########################################
# loop over all files in files loop
#for AFIL in $PARAMETERS
#do
   echo "now evaluating parameter run $AFIL"
python $SCRIPTDIR/dmClumps.merge_STATtestspectra-14-02-05.py -p $psiZeroDeg -t $thetaZeroDeg -w $psiWidthDeg -v $thetaWidthDeg -a $alphaIntDeg -u $user_rse -o $WORKDIR -r $outdir &
#$TOYDIR/clumpyMC.prepareSkymap.py 
#done

exit

