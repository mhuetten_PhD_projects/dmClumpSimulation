#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

###############################################################################
# Python script for creating the High resolution 2D sky patch figures
# for the ClumpyV2 paper.

###############################################################################
##    import modules:    ######################################################
###############################################################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import *
import pylab
import healpy as hp
import bisect
from matplotlib import rc,rcParams

import matplotlib.ticker as ticker
from matplotlib.ticker import LogLocator
###############################################################################

rc('text',usetex=True)

print 'load data...'
#data_unsmoothed = np.loadtxt('/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-05-26-PublicationPlots/HighResPatchSpherical/annihil_gal2D_LOS180,0_FOV5x5_rse1_alphaint0.01deg_nside8192-JFACTOR_PER_SR.dat')
#data_smoothed = np.loadtxt('/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-05-26-PublicationPlots/HighResPatchSpherical/annihil_gal2D_LOS180,0_FOV5x5_rse1_alphaint0.01deg_nside8192-JTOT_SMOOTHED.dat')
data_Jfactor = np.loadtxt('/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-05-26-PublicationPlots/HighResPatchSpherical/annihil_gal2D_LOS180,0_FOV5x5_rse1_alphaint0.01deg_nside8192-JFACTOR.dat')


#data = data_unsmoothed
#data = data_smoothed
data = data_Jfactor

pdf_name = 'HighresPatchSpherical-unsmoothed.pdf'
pdf_name = 'HighresPatchSpherical-smoothed.pdf'
pdf_name = 'HighresPatchSpherical-JfactorAstroUnits.pdf'

column = 2

Gev2bycm5toMsol2bykpc5 = 2.248370e-7
 
datalength = len(data[:,-1])
print " have", datalength, "pixels in clumpy map"

nPsi = 1000
nTheta = 1000

NSIDE = 8192

psi_min = 360.
psi_max = 0.
theta_min = 90.
theta_max = -90.

deg2rad = np.pi / 180.

for j in range(0,datalength):
    if data[j,0] < 0.:
        data[j,0] += 360.
    if data[j,0] < psi_min:
        psi_min = data[j,0]
    if data[j,0] > psi_max:
        psi_max = data[j,0]
    if data[j,1] < theta_min:
        theta_min = data[j,1]
    if data[j,0] > theta_max:
        theta_max = data[j,1]
    #data[j,0] *= deg2rad
    #data[j,1] = np.pi - data[j,1] *deg2rad

print "psi_min:",psi_min
print "psi_max",psi_max 
print "theta_min:",theta_min
print "theta_max",theta_max 

datasorted = data[np.argsort(data[:, -1])] #
print datasorted

delta_psi = (psi_max - psi_min) / nPsi
delta_theta = (theta_max - theta_min) / nTheta

print " # pixels in psi direction:",nPsi,"pixels"

print " # pixels in theta direction:",nTheta,"pixels"
print ""
# 
# extract psi, theta vector:
psiVector = np.zeros(nPsi)
thetaVector = np.zeros(nTheta)

psiVector_rad = np.zeros(nPsi)
thetaVector_rad = np.zeros(nTheta)

for j in range(nPsi):
    psiVector[j] = psi_min + j * delta_psi
    psiVector_rad[j] = psiVector[j] *deg2rad
for k in range(nTheta):    
    thetaVector[k] = theta_min + k * delta_theta
    thetaVector_rad[k] = (90. - thetaVector[k]) * deg2rad
 
# MAX = max(data[:,6])
# 
# x = np.zeros(datalength)
# y = np.zeros(datalength)
# z = np.zeros(datalength)
# # 
pixnr_array = datasorted[:,-1]
pixnr_list = list(pixnr_array)

minval = min(datasorted[:,column])
print minval

plotdata = np.zeros(shape=(nTheta,nPsi))
for k in range(nTheta):
        print "row", k, "of", nTheta        
        for j in range(nPsi):
            pixelnr = hp.ang2pix(NSIDE, thetaVector_rad[k], psiVector_rad[j])
            #itemindex = np.argmax(datasorted[:,-1]==pixelnr)
            
            itemindex = bisect.bisect(datasorted[:,-1], pixelnr)
            if itemindex > datalength -1:
                plotdata[k,j] = minval * Gev2bycm5toMsol2bykpc5
            #dist = hp.rotator.angdist([psiVector[j] ,thetaVector[k]],[data[0,0], data[0,1]], lonlat=True) 
            #print  dist / np.pi * 180. 
            else:
                plotdata[k,j] =  datasorted[itemindex,column] * Gev2bycm5toMsol2bykpc5

rcParams.update({'font.size': 18}) 

# #colormap_r = ListedColormap(Blues.colors[::-1])


fig = plt.imshow(plotdata,norm=LogNorm(vmin=minval * Gev2bycm5toMsol2bykpc5, vmax=1e11), extent=[psi_min,psi_max,theta_min,theta_max], interpolation="nearest")#, colormap='gist_earth')
#ax.set_axis_off()
#pylab.savefig('foopink.png', bbox_inches=0)
fig.set_cmap('gist_earth')
#plt.axis('off')
 

def fmt(x,pos):
    #a, b = '{:.2e}'.format(x).split('e')
    #b = int(b)
    coefficient, exponent = convertToScientific(x)
    if np.log10(x).is_integer() == True:
        #return r'$%.1e$' % (x)
        return r'$%1.0f \times 10^{%d}$' %(coefficient, exponent)
    else:
        return ''
        #return r'$%1.0f \times 10^{%d}$' %(coefficient, exponent)

def convertToScientific(nr):
    coefficient = float(nr)
    exponent = 0
    while abs(coefficient) >= 10:
        exponent += 1
        coefficient = coefficient / 10
    while abs(coefficient) < 1:
        exponent -= 1
        coefficient = coefficient * 10
    return (coefficient, exponent)


cbar = plt.colorbar(fig, ticks = LogLocator(subs=range(10)), format=ticker.FuncFormatter(fmt))
#cbar = plt.colorbar(fig)
#tick_locator = ticker.MaxNLocator(nbins=5)
#cbar.locator = tick_locator
#cbar.update_ticks()
#cbar.ax.set_yticklabels([r'$10^8$','','','','','','','','',r'$10^9$'])
#cbar.set_label(r'$\rm{[GeV^{\;2}\;cm^{-5}\;sr^{-1}]}$', rotation=270)
cbar.set_label(r'$\rm{[M_{\odot}^{\;2}\;kpc^{-5}]}$', rotation=270)

rcParams.update({'font.size': 18})
plt.rc('font', family='serif')
pylab.xlabel(r"Galactic longitude $l$ [degrees]", labelpad=10)
pylab.ylabel(r"Galactic latitude $b$ [degrees]")
#pylab.title('Brezovich Criterion',{'fontsize':20})
pylab.grid()
plt.savefig(pdf_name, format='pdf', dpi=300)

plt.show()
