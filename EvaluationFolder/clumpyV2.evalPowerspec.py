#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import pyplot as plt
import pylab
# warning: due to a bug in healpy, importing it before pylab can cause
#  a segmentation fault in some circumstances.
import healpy as hp
import random as rn
import pickle 
import time
import os, glob
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from matplotlib.patches import Rectangle
from textwrap import wrap
import shutil # for file copying
import pyfits as pf
from matplotlib import rc,rcParams

rc('text',usetex=True)

def main(argv):
    
    ###########################################################################
    # read input variables

    indir2 = "empty"
    lmax_plot = 1000
    
    line_width = 2
    text_size = 18
    inline_fontsize = 18
    
    plottype = 'dpdv'
    
    if plottype == 'cvirscatter':
        labelmodel9 = r"{model 9: $\sigma_c = 0.24$}"
        labelmodelcompare = r"{model 1: $\sigma_c = 0.14$}"
        smooth_label = "smooth halo"
        
    if plottype == 'cvir':
        labelmodel9 = r"{model 9: $c({ M_{sub}})$ SANCHEZ}"
        labelmodelcompare = r"{model 13: $c({ M_{sub}})$ BULLOCK}"
        smooth_label = "smooth halo"
        
    if plottype == 'dpdm':
        labelmodel9 = r"{model 9: $\alpha_m=1.9$}"
        labelmodelcompare = r"{model 11: $\alpha_m=2.0$}"
        smooth_label = "smooth haloes"
        
    if plottype == 'dpdv':
        labelmodel9 = r"{model 9: $\varrho_{ sub}$ MADAU}"
        labelmodelcompare = r"{model 10: $\varrho_{ sub}$ EINASTO}"
        smooth_label = "smooth haloes"

    try:
        opts, args = getopt.getopt(sys.argv[1:],"i:j:l:",["indir1=","indir2=","lmax="])
    except getopt.GetoptError:
        # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
        # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
        print 'The input options are:'
        print 'xxx'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            # help option
            print 'The input options are:'
            print 'xxx'
            sys.exit()
        elif opt in ("-i", "--indir1"):
            indir1 = str(arg)
        elif opt in ("-j", "--indir2"):
            indir2 = str(arg)
        elif opt in ("-l", "--lmax"):
            lmax_plot = int(arg)

    
    print " initializing script clumpyV2.plotPowerSpec.py"

    ###########################################################################
    # read data:

    ##### folder 1:
    nfiles = 0
    
    os.chdir(indir1)
    for name in glob.glob('*/powerspec*')+glob.glob('powerspec*'):
        nfiles = nfiles + 1

    print " number of power spectra in subfolder 1: ", nfiles

    intensityAPScombined_raw   = []
    fluctuationAPScombined_raw = []
    
    ifile = 0
    for name in glob.glob('*/powerspec*')+glob.glob('powerspec*'):
        powspec_raw = hp.mrdfits(name)
        powspec_tot     = powspec_raw[0].flatten()
        powspec_smooth  = powspec_raw[1].flatten()
        powspec_drawn   = powspec_raw[2].flatten()
        powspec_substot = powspec_raw[3].flatten()

        hdulist = pf.open(name)
        alpha_int1     = hdulist[1].header['ALPHAINT']
        j_tot_mean     = hdulist[1].header['JTOT_M']
        j_sm_mean      = hdulist[1].header['JSM_M']
        j_drawn_mean   = hdulist[1].header['JDRAWN_M']
        j_substot_mean = hdulist[1].header['JSUBS_M']
        fsky           = hdulist[1].header['F_SKY']
        hdulist.close()

        intensityAPScombined_raw.append(powspec_substot / fsky)        
        fluctuationAPScombined_raw.append(fsky * powspec_substot/(j_substot_mean)**2)


    lmax1 = len(intensityAPScombined_raw[0])
    print " length of first spectrum in 2D array: ", lmax1
    intensityAPScombined1   = np.ndarray((nfiles, lmax1))
    fluctuationAPScombined1 = np.ndarray((nfiles, lmax1))
    for n in range(nfiles):
        intensityAPScombined1[n,:]   = intensityAPScombined_raw[n]
        fluctuationAPScombined1[n,:] = fluctuationAPScombined_raw[n]

    # calculate mean and standard deviation for each l by assuming normal distributed values:
    print "calculate mean and standard deviation for each l:"
    intensityAPSaverage1 = np.ndarray((2, lmax1))
    fluctuationAPSaverage1 = np.ndarray((2, lmax1))
    intensityAPSsmooth1 = powspec_smooth / fsky
    fluctuationAPSsmooth1 = powspec_smooth * fsky / (j_sm_mean)**2
    
    for l in range(lmax1):
        intensityAPSaverage1[0,l] = 10**(np.mean(np.log10(intensityAPScombined1[:,l])))
        intensityAPSaverage1[1,l] = 10**(np.std(np.log10(intensityAPScombined1[:,l])))
        fluctuationAPSaverage1[0,l] = 10**(np.mean(np.log10(fluctuationAPScombined1[:,l])))
        fluctuationAPSaverage1[1,l] = 10**(np.std(np.log10(fluctuationAPScombined1[:,l])))

    if indir2 != "empty":
        ##### folder 2:
        nfiles = 0
        
        os.chdir(indir2)
        for name in glob.glob('*/powerspec*')+glob.glob('powerspec*'):
            nfiles = nfiles + 1
    
        print " number of power spectra in subfolder 2: ", nfiles
    
        intensityAPScombined_raw = []
        fluctuationAPScombined_raw = []
        
        ifile = 0
        for name in glob.glob('*/powerspec*')+glob.glob('powerspec*'):
            powspec_raw = hp.mrdfits(name)
            powspec_tot     = powspec_raw[0].flatten()
            powspec_smooth  = powspec_raw[1].flatten()
            powspec_drawn   = powspec_raw[2].flatten()
            powspec_substot = powspec_raw[3].flatten()

            hdulist = pf.open(name)
            alpha_int2     = hdulist[1].header['ALPHAINT']
            j_tot_mean     = hdulist[1].header['JTOT_M']
            j_sm_mean      = hdulist[1].header['JSM_M']
            j_drawn_mean   = hdulist[1].header['JDRAWN_M']
            j_substot_mean = hdulist[1].header['JSUBS_M']
            fsky           = hdulist[1].header['F_SKY']
            hdulist.close()

            intensityAPScombined_raw.append(powspec_substot / fsky)        
            fluctuationAPScombined_raw.append(fsky * powspec_substot/(j_substot_mean)**2)
            
    
        lmax2 = len(intensityAPScombined_raw[0])
        print " length of first spectrum in 2D array: ", lmax2
        
        intensityAPScombined2 = np.ndarray((nfiles, lmax2))
        fluctuationAPScombined2 = np.ndarray((nfiles, lmax2))
        
        for n in range(nfiles):
            intensityAPScombined2[n,:] = intensityAPScombined_raw[n]
            fluctuationAPScombined2[n,:] = fluctuationAPScombined_raw[n]
    
        # calculate mean and standard deviation for each l by assuming normal distributed values:
        print "calculate mean and standard deviation for each l:"
        intensityAPSaverage2 = np.ndarray((2, lmax2))
        fluctuationAPSaverage2 = np.ndarray((2, lmax2))
        intensityAPSsmooth2 = powspec_smooth / fsky
        fluctuationAPSsmooth2 = powspec_smooth * fsky / (j_sm_mean)**2
        
        for l in range(lmax1):
            intensityAPSaverage2[0,l] = 10**(np.mean(np.log10(intensityAPScombined2[:,l])))
            intensityAPSaverage2[1,l] = 10**(np.std(np.log10(intensityAPScombined2[:,l])))
            fluctuationAPSaverage2[0,l] = 10**(np.mean(np.log10(fluctuationAPScombined2[:,l])))
            fluctuationAPSaverage2[1,l] = 10**(np.std(np.log10(fluctuationAPScombined2[:,l])))

    
    print "plotting clumpy power spectra..."

    ###########################################################################
    # plot intensity APS

    delta_omega1 = 2.*np.pi * (1. -np.cos(alpha_int1 /180. * np.pi))
    delta_omega2 = 2.*np.pi * (1. -np.cos(alpha_int2 /180. * np.pi))

    fluxfactor = 5.628471e-32 # GeV^{-3} cm^{3} s^{-1}
    
                
    intensityAPSaverage1[0] *= (fluxfactor / delta_omega1  )**2
    intensityAPSsmooth1 *= (fluxfactor / delta_omega1  )**2
    
    if indir2 != "empty":
        intensityAPSaverage2[0] *= (fluxfactor / delta_omega2  )**2
        intensityAPSsmooth2 *= (fluxfactor / delta_omega2  )**2

   
    plot_intensityAPS = plt.figure(figsize=(8,5))

    if lmax2 > lmax1:
        lmax2 = lmax1

    ell1 = np.arange(lmax1)
    ell2 = np.arange(lmax2)
    
    #for n in range(nfiles):
    #        plt.plot(ell1, ell1 * (ell1+1) * (APScombined[n,:])/(2*np.pi),color='blue',  linestyle='-', linewidth=1., label=r"Mean")#, label="realisation "+str(runnumbers[i]+1))

    p1 = plt.plot(ell1, ell1 * (ell1+1) * (intensityAPSaverage1[0,:])/(2*np.pi),color='black',  linestyle='-', linewidth=line_width, label=labelmodel9)#, label="realisation "+str(runnumbers[i]+1))
    p2 = plt.fill_between(ell1, ell1 * (ell1+1) * ((intensityAPSaverage1[0,:]) *  (intensityAPSaverage1[1,:]))/(2*np.pi),ell1 * (ell1+1) * ((intensityAPSaverage1[0,:]) /  (intensityAPSaverage1[1,:]))/(2*np.pi), color='black', alpha=0.4)        
    
    if indir2 != "empty":
        p4 = plt.plot(ell2, ell2 * (ell2+1) * (intensityAPSaverage2[0,0:lmax2])/(2*np.pi),color='blue',  linestyle='--', linewidth=line_width, label=labelmodelcompare)#, label="realisation "+str(runnumbers[i]+1))
        p5 = plt.fill_between(ell2, ell2 * (ell2+1) * ((intensityAPSaverage2[0,0:lmax2]) *  (intensityAPSaverage2[1,0:lmax2]))/(2*np.pi),ell2 * (ell2+1) * ((intensityAPSaverage2[0,0:lmax2]) /  (intensityAPSaverage2[1,0:lmax2]))/(2*np.pi), color='blue', alpha=0.4)
        #p6 = plt.plot(ell1, ell1 * (ell1+1) * intensityAPSsmooth2/(2*np.pi),color='blue', alpha=0.6,  linestyle='-.', linewidth=line_width, label="smooth halo 2") 

    p3 = plt.plot(ell1, ell1 * (ell1+1) * intensityAPSsmooth1/(2*np.pi),color='black', alpha=0.6,  linestyle='-.', linewidth=line_width, label=smooth_label) 

    #p6 = plt.plot(ell1, ell1 * (ell1+1) * 10**(-22),color='grey',  linestyle='-.', linewidth=2, label="Poissonian noise (arbitr. norm.)") 
 
    plt.rc('font', family='serif')
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='lower right',prop={'size':inline_fontsize})
 
    plt.xlabel('multipole $l$',  labelpad=0)
    plt.ylabel(r"$l(l+1)C_l/2\pi\;\rm{[cm^{-4}\,s^{-2}\,sr^{-2}\,GeV^{-2}]}$",  labelpad=7);
    plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,lmax_plot])
    pylab.ylim([1e-24,1e-18])
    rcParams.update({'font.size':text_size})
    
    os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")    
    pdf_name = 'APS-Intensity-'+plottype+'.pdf'
    plt.savefig(pdf_name, format='pdf')


    ###########################################################################
    # plot fluctuation APS
   
    plot_fluctuationAPS = plt.figure(figsize=(8,5))
 
    p1 = plt.plot( fluctuationAPSaverage1[0,:], color='black', linestyle='-', linewidth=line_width, label=labelmodel9)#, label="realisation "+str(runnumbers[i]+1))
    p2 = plt.fill_between(ell1, ( fluctuationAPSaverage1[0,:] * fluctuationAPSaverage1[1,:]), ( fluctuationAPSaverage1[0,:]  / fluctuationAPSaverage1[1,:]), color='black', alpha=0.4)        

    if indir2 != "empty":
        p4 = plt.plot( fluctuationAPSaverage2[0,0:lmax2], color='blue', linestyle='--', linewidth=line_width, label=labelmodelcompare)#, label="realisation "+str(runnumbers[i]+1))
        p5 = plt.fill_between(ell2, ( fluctuationAPSaverage2[0,0:lmax2] * fluctuationAPSaverage2[1,0:lmax2]), ( fluctuationAPSaverage2[0,0:lmax2]  / fluctuationAPSaverage2[1,0:lmax2]), color='blue', alpha=0.4)        
        #p6 = plt.plot(ell1, fluctuationAPSsmooth2, color='blue', alpha=0.6,  linestyle='-.', linewidth=2.5, label="smooth halo 2") 

    p3 = plt.plot(ell1, fluctuationAPSsmooth1, color='black', alpha=0.6,  linestyle='-.', linewidth=line_width, label=smooth_label) 

    #p6 = plt.plot(ell1, ell1 * (ell1+1) * 10**(-22),color='grey',  linestyle='-.', linewidth=2, label="Poissonian noise (arbitr. norm.)") 

    plt.rc('font', family='serif')
    plt.subplots_adjust(top=0.85)
    plt.legend(loc='lower left',prop={'size':inline_fontsize})

    plt.xlabel('multipole $l$',  labelpad=0); 
    plt.ylabel(r"$\hat{C}_l\;\rm{[unitless]}$",  labelpad=7);
    plt.grid()
    plt.xscale('log')
    plt.yscale('log')
    pylab.xlim([1,lmax_plot])
    pylab.ylim([1e-7,1])
    rcParams.update({'font.size': text_size})
    
    os.chdir("/afs/ifh.de/user/m/mhuetten/projects/dmClumpSimulation/Plots")  
    pdf_name = 'APS-Fluctuation-'+plottype+'.pdf'
    plt.savefig(pdf_name, format='pdf')
    plt.show()
    
    
    
    
    
    print "finished."
    print ""
    
if __name__ == "__main__":
     
    main(sys.argv[1:])

##    end of file    ##########################################################
###############################################################################    
