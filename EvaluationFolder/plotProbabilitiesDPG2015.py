from matplotlib import pyplot as plt
import pylab
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
import numpy as np
from matplotlib import rc,rcParams
from scipy import stats


rc('text',usetex=True)
rcParams.update({'font.size': 16})

#################
 
# single clumps probability (I): the influence of clumps dPdV and concentration
  
# read files:
directory ="/afs/ifh.de/group/cta/scratch/mhuetten/Clumpy_stat_results/"
dataFornasaAquarius= np.loadtxt(directory+"FornasaAquarius/annihil_gal2D_LOS0,0_FOV360x-60_rse5_alphaint0.12deg_nside512_Nrep100.stat")#, comments="#", names=True)
dataLangeViaLactea= np.loadtxt(directory+"LangeViaLactea/annihil_gal2D_LOS0,0_FOV360x-60_rse5_alphaint0.12deg_nside512_Nrep100.stat")#, comments="#", names=True)


  
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax2 = ax1.twiny()
  
  
ax1.plot(dataFornasaAquarius[25:-1,0],dataFornasaAquarius[25:-1,5],color='blue',  linestyle='-', linewidth=1., label=r"Fornasa 2013 (Aquarius)") 
ax1.fill_between(dataFornasaAquarius[25:-1,0],dataFornasaAquarius[25:-1,5]+dataFornasaAquarius[25:-1,6],dataFornasaAquarius[25:-1,5]-dataFornasaAquarius[25:-1,6], color='blue', alpha=0.4)
ax1.plot(dataLangeViaLactea[25:-1,0],dataLangeViaLactea[25:-1,5],color='darkred',  linestyle='-', linewidth=1., label=r"Lange 2015 (Via Lactea)") 
ax1.fill_between(dataLangeViaLactea[25:-1,0],dataLangeViaLactea[25:-1,5]+dataLangeViaLactea[25:-1,6],dataLangeViaLactea[25:-1,5]-dataLangeViaLactea[25:-1,6], color='darkred', alpha=0.4) 
  

ax1.legend(loc='lower left',prop={'size':14})
ax1.set_ylabel(r'$p(\geq 1\,\mathrm{clump}(\geq J))$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
#plt.xscale('lin')
ax1.set_yscale('log')
ax1.yaxis.grid();
#pylab.xlim([10,14])
pylab.ylim([5e-3,1.2])

new_tick_locations = np.array([.0,.4,  .8])
new_ticks = [r"$10^{-5}\,\mathrm{C.U.}$", r"$10^{-4}\,\mathrm{C.U.}$", r"$10^{-3}\,\mathrm{C.U.}$"]
  
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(new_ticks)
ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\tau^+\tau^-,\,\langle\sigma v\rangle = 3\cdot 10^{-26}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")


# fig2 = plt.figure()
# ax1 = fig2.add_subplot(111)
# ax2 = ax1.twiny()
# 
# ax1.plot(dataZhaoSummer2014[5:-5,0],dataZhaoSummer2014[5:-5,5],color='green',  linestyle='-', linewidth=1., label=r"biased dP/dV, cvir HIGH (summer 2014)") 
# ax1.fill_between(dataZhaoSummer2014[5:-5,0],dataZhaoSummer2014[5:-5,5]+dataZhaoSummer2014[5:-5,6],dataZhaoSummer2014[5:-5,5]-dataZhaoSummer2014[5:-5,6], color='green', alpha=0.4)
#  
# ax1.legend(loc='lower left',prop={'size':14})
# ax1.set_ylabel(r'$p(\geq 1\,\mathrm{clump}(\geq J))$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
# #plt.xscale('lin')
# ax1.set_yscale('log')
# ax1.yaxis.grid(); 
# new_tick_locations = np.array([.1625, .4125, .6625 , .9125])
# new_ticks = [r"$10^{-7}\,\mathrm{C.U.}$", r"$10^{-6}\,\mathrm{C.U.}$", r"$10^{-5}\,\mathrm{C.U.}$", r"$10^{-4}\,\mathrm{C.U.}$"]
#  
# ax2.set_xticks(new_tick_locations)
# ax2.set_xticklabels(new_ticks)
# ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\ b\bar{b},\,\langle\sigma v\rangle = 3\cdot 10^{-24}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")
# 
# 
# 
#   
#   
####################
 
# luminosity population (I): the influence of clumps dPdV and concentration
 
fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
ax2 = ax1.twiny()
  
ax1.plot(dataFornasaAquarius[10:-2,0],dataFornasaAquarius[10:-2,3],color='blue',  linestyle='-', linewidth=1., label=r"Fornasa 2013 (Aquarius)") 
ax1.fill_between(dataFornasaAquarius[10:-2,0],dataFornasaAquarius[10:-2,3]+dataFornasaAquarius[10:-2,4],dataFornasaAquarius[10:-2,3]-dataFornasaAquarius[10:-2,4], color='blue', alpha=0.4)
ax1.plot(dataLangeViaLactea[10:-2,0],dataLangeViaLactea[10:-2,3],color='darkred',  linestyle='-', linewidth=1., label=r"Lange 2015 (Via Lactea)") 
ax1.fill_between(dataLangeViaLactea[10:-2,0],dataLangeViaLactea[10:-2,3]+dataLangeViaLactea[10:-2,4],dataLangeViaLactea[10:-2,3]-dataLangeViaLactea[10:-2,4], color='darkred', alpha=0.4) 
  
ax1.legend(loc='lower left',prop={'size':14})
ax1.set_ylabel(r'$<N_{cumul}>(\geq J)$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
#plt.xscale('lin')
ax1.set_yscale('log')
ax1.yaxis.grid();
ax1.set_ylim((1e-2,2e2))
 
new_tick_locations = np.array([.125,.375,.625,  .875])
new_ticks = [r"$10^{-6}\,\mathrm{C.U.}$",r"$10^{-5}\,\mathrm{C.U.}$", r"$10^{-4}\,\mathrm{C.U.}$", r"$10^{-3}\,\mathrm{C.U.}$"]
  
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(new_ticks)
ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\tau^+\tau^-,\,\langle\sigma v\rangle = 3\cdot 10^{-26}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")

# 
# ## fit:
# slope, intercept, r_value, p_value, std_err = stats.linregress(dataEinastoAntiLavalle[5:-30,0],np.log10(dataEinastoAntiLavalle[5:-30,3]))
# print "slope dataEinastoAntiLavalle", slope, " error on slope", std_err
# 
# slope, intercept, r_value, p_value, std_err = stats.linregress(dataEinastoAntiSanchez[5:-30,0],np.log10(dataEinastoAntiSanchez[5:-30,3]))
# print "slope dataEinastoAntiSanchez", slope, " error on slope", std_err
# 
# slope, intercept, r_value, p_value, std_err = stats.linregress(dataGaoAntiLavalle[5:-30,0],np.log10(dataGaoAntiLavalle[5:-30,3]))
# print "slope dataGaoAntiLavalle", slope, " error on slope", std_err
# 
# slope, intercept, r_value, p_value, std_err = stats.linregress(dataGaoAntiSanchez[5:-30,0],np.log10(dataGaoAntiSanchez[5:-30,3]))
# print "slope dataGaoAntiSanchez", slope, " error on slope", std_err
# 
# slope, intercept, r_value, p_value, std_err = stats.linregress(dataZhaoSummer2014[10:-20,0],np.log10(dataZhaoSummer2014[10:-20,3]))
# print "slope dataZhaoSummer2014", slope, " error on slope", std_err
# 

####################

# Small FOV
   
#################
 
# single clumps probability (I): the influence of clumps dPdV and concentration
  
# read files:
directory ="/afs/ifh.de/group/cta/scratch/mhuetten/Clumpy_stat_results/"
dataFornasaAquarius= np.loadtxt(directory+"FornasaAquarius/annihil_gal2D_LOS0,90_FOVdiameter3.5deg_rse0_alphaint0.12deg_nside512_Nrep500.stat")#, comments="#", names=True)
dataLangeViaLactea= np.loadtxt(directory+"LangeViaLactea/annihil_gal2D_LOS0,90_FOVdiameter3.5deg_rse0_alphaint0.12deg_nside512_Nrep500.stat")#, comments="#", names=True)


  
fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax2 = ax1.twiny()
  
  
ax1.plot(dataFornasaAquarius[0:-22,0],dataFornasaAquarius[0:-22,5],color='blue',  linestyle='-', linewidth=1., label=r"Fornasa 2013 (Aquarius)") 
ax1.fill_between(dataFornasaAquarius[0:-22,0],dataFornasaAquarius[0:-22,5]+dataFornasaAquarius[0:-22,6],dataFornasaAquarius[0:-22,5]-dataFornasaAquarius[0:-22,6], color='blue', alpha=0.4)
ax1.plot(dataLangeViaLactea[0:-22,0],dataLangeViaLactea[0:-22,5],color='darkred',  linestyle='-', linewidth=1., label=r"Lange 2015 (Via Lactea)") 
ax1.fill_between(dataLangeViaLactea[0:-22,0],dataLangeViaLactea[0:-22,5]+dataLangeViaLactea[0:-22,6],dataLangeViaLactea[0:-22,5]-dataLangeViaLactea[0:-22,6], color='darkred', alpha=0.4) 
  

ax1.legend(loc='lower left',prop={'size':14})
ax1.set_ylabel(r'$p(\geq 1\,\mathrm{clump}(\geq J))$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
#plt.xscale('lin')
ax1.set_yscale('log')
ax1.yaxis.grid();
#pylab.xlim([10,14])
pylab.ylim([5e-3,1.2])

new_tick_locations = np.array([.1666,.5,  .833])
new_ticks = [r"$10^{-7}\,\mathrm{C.U.}$", r"$10^{-6}\,\mathrm{C.U.}$", r"$10^{-5}\,\mathrm{C.U.}$"]
  
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(new_ticks)
ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\tau^+\tau^-,\,\langle\sigma v\rangle = 3\cdot 10^{-26}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")


# fig2 = plt.figure()
# ax1 = fig2.add_subplot(111)
# ax2 = ax1.twiny()
# 
# ax1.plot(dataZhaoSummer2014[5:-5,0],dataZhaoSummer2014[5:-5,5],color='green',  linestyle='-', linewidth=1., label=r"biased dP/dV, cvir HIGH (summer 2014)") 
# ax1.fill_between(dataZhaoSummer2014[5:-5,0],dataZhaoSummer2014[5:-5,5]+dataZhaoSummer2014[5:-5,6],dataZhaoSummer2014[5:-5,5]-dataZhaoSummer2014[5:-5,6], color='green', alpha=0.4)
#  
# ax1.legend(loc='lower left',prop={'size':14})
# ax1.set_ylabel(r'$p(\geq 1\,\mathrm{clump}(\geq J))$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
# #plt.xscale('lin')
# ax1.set_yscale('log')
# ax1.yaxis.grid(); 
# new_tick_locations = np.array([.1625, .4125, .6625 , .9125])
# new_ticks = [r"$10^{-7}\,\mathrm{C.U.}$", r"$10^{-6}\,\mathrm{C.U.}$", r"$10^{-5}\,\mathrm{C.U.}$", r"$10^{-4}\,\mathrm{C.U.}$"]
#  
# ax2.set_xticks(new_tick_locations)
# ax2.set_xticklabels(new_ticks)
# ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\ b\bar{b},\,\langle\sigma v\rangle = 3\cdot 10^{-24}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")
# 
# 
# 
#   
####################
 
# luminosity population (I): the influence of clumps dPdV and concentration
 
fig4 = plt.figure()
ax1 = fig4.add_subplot(111)
ax2 = ax1.twiny()
  
ax1.plot(dataFornasaAquarius[0:-22,0],dataFornasaAquarius[0:-22,3],color='blue',  linestyle='-', linewidth=1., label=r"Fornasa 2013 (Aquarius)") 
ax1.fill_between(dataFornasaAquarius[0:-22,0],dataFornasaAquarius[0:-22,3]+dataFornasaAquarius[0:-22,4],dataFornasaAquarius[0:-22,3]-dataFornasaAquarius[0:-22,4], color='blue', alpha=0.4)
ax1.plot(dataLangeViaLactea[0:-22,0],dataLangeViaLactea[0:-22,3],color='darkred',  linestyle='-', linewidth=1., label=r"Lange 2015 (Via Lactea)") 
ax1.fill_between(dataLangeViaLactea[0:-22,0],dataLangeViaLactea[0:-22,3]+dataLangeViaLactea[0:-22,4],dataLangeViaLactea[0:-22,3]-dataLangeViaLactea[0:-22,4], color='darkred', alpha=0.4) 
  
ax1.legend(loc='lower left',prop={'size':14})
ax1.set_ylabel(r'$<N_{cumul}>(\geq J)$'); ax1.set_xlabel(r'$log_{10}(J/M_{\odot}^2\,kpc^{-5})$'); plt.grid()
#plt.xscale('lin')
ax1.set_yscale('log')
ax1.yaxis.grid();
ax1.set_ylim((1e-2,2e1))
 
new_tick_locations = np.array([.1666,.5,  .833])
new_ticks = [r"$10^{-7}\,\mathrm{C.U.}$", r"$10^{-6}\,\mathrm{C.U.}$", r"$10^{-5}\,\mathrm{C.U.}$"]
  
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(new_ticks)
ax2.set_xlabel(r"$\mathrm{flux\;>\,250\,GeV\; for\; \chi\rightarrow\tau^+\tau^-,\,\langle\sigma v\rangle = 3\cdot 10^{-26}\, cm^3 s^{-1},\,m_{\chi}=500\,GeV}$")


plt.show()
