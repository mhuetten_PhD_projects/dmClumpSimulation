#!/usr/bin/python
# -*- coding: utf-8 -*-

##    Author: Moritz Huetten, moritz.huetten@desy.de    #######################
###############################################################################

###############################################################################
# Python script for creating the 2D Full sky figures
# for the ClumpyV2 paper.
#
# 1.) Add the flux extension with the pp model in the clumpyV2.clumpy_params.txt
#     via the -f option.
# 2.) export the required map with the -o2 option from the fits file
# 3.) Then run this Python script (some parameters have to be adjusted by hand
#     below in this script)
#
###############################################################################
##    import modules:    ######################################################
###############################################################################
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib.colors import *
import pylab
import healpy as hp
import bisect
from matplotlib import rc,rcParams
import matplotlib.ticker as ticker
from matplotlib.ticker import LogLocator
from matplotlib import cm
import MoritzSphericalTools
rc('text',usetex=True)
###############################################################################

print 'load data'
mapname = '/lustre/fs5/group/cta/users/mhuetten/workdata/dmClumpSimulation/15-05-26-PublicationPlots/FullskySpherical-RSE-2/annihil_gal2D_LOS0,0_FOV360x180_rse2_alphaint0.12deg_nside512-JFACTOR_PER_SR-Jtot.fits'
pdf_name = 'Fullskymap-Spherical-Jfactor.png'

image_resolution = 1200

###############################################################################
# no editing needed below this line
###############################################################################

map = hp.read_map(mapname)

print "NSIDE:",hp.npix2nside(len(map))

plot_skymap = plt.figure(figsize=(12,10))

my_cmap = cm.gist_earth
my_cmap.set_under("w") # sets background to white

minval = min(map)

def fmt(x,pos):
    coefficient, exponent = convertToScientific(x)
    if np.log10(x).is_integer() == True:
        #return r'$%.1e$' % (x)
        #return r'$%1.0f \times 10^{%d}$' %(coefficient, exponent)
        if x > minval:
            return r'$10^{%d}$' %(exponent)
        else:
            return ''
    else:
        return ''
        #return r'$%1.0f \times 10^{%d}$' %(coefficient, exponent)

def convertToScientific(nr):
    coefficient = float(nr)
    exponent = 0
    while abs(coefficient) >= 10:
        exponent += 1
        coefficient = coefficient / 10
    while abs(coefficient) < 1:
        exponent -= 1
        coefficient = coefficient * 10
    return (coefficient, exponent)

rcParams.update({'font.size': 28}) 

fig = hp.mollview(map, fig=1, title='',coord=['G','C'], min = minval, max= 1e24, norm='log', flip='geo', xsize=image_resolution, cmap=my_cmap, cbar=None)

hp.graticule(dpar=180./12., dmer=180./8., coord='C')

npoints = 81
thetaCenter = np.pi
psiCenter = 0.
fovRadius = 60./180*np.pi
fovTheta = np.zeros(npoints)
fovPsi = np.zeros(npoints)
for i in range(npoints):
    phi = np.pi/20 * i
    fovTheta[i], fovPsi[i] = MoritzSphericalTools.polarCoordinatesOnSphere(thetaCenter, psiCenter, fovRadius, phi)
hp.projplot(fovPsi/np.pi*180, 90-fovTheta/np.pi*180,'k-', lonlat=True, coord='G',linewidth=1.5, color='r', linestyle='--')

npoints = 1001
thetaCenter = np.pi
psiCenter = 0.
fovRadius = 90./180*np.pi
fovTheta = np.zeros(npoints)
fovPsi = np.zeros(npoints)
for i in range(npoints):
    phi = np.pi/20 * i
    fovTheta[i], fovPsi[i] = MoritzSphericalTools.polarCoordinatesOnSphere(thetaCenter, psiCenter, fovRadius, phi)
hp.projplot(fovPsi/np.pi*180, 90-fovTheta/np.pi*180,'k-', lonlat=True, coord='G',linewidth=1, color='k', linestyle='-')



fig = plt.gcf()
ax = plt.gca()
image = ax.get_images()[0]
cbaxes = fig.add_axes([0.1, 0.15, 0.8, 0.03]) 
cbar = fig.colorbar(image, ax=ax, orientation="horizontal", ticks = LogLocator(subs=range(10)), format=ticker.FuncFormatter(fmt), cax = cbaxes)
#[left, bottom, width, height]
#cbar.set_label(r'$\rm{[cm^{-2}\,s^{-1}\,sr^{-1}\,GeV^{-1}]}$', labelpad=10)
cbar.set_label(r'$\rm{[GeV^{5}\,cm^{-2}\,sr^{-1}]}$', labelpad=15)

plt.savefig(pdf_name, format='png', dpi=300)

plt.show()

