# -*- coding: utf-8 -*-

#######################################
# Changelog:
#14/1/27: Corrected expression for healpix pixel resolution
#         now the resolution corresponds to the full diameter
#         of the pixel, which are assumed being circular.
######################################




import numpy as np
from numpy import loadtxt
from matplotlib import pyplot as plt
import pylab  
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.
import os
import sys
import getopt
from matplotlib.backends.backend_pdf import PdfPages as pdf
from textwrap import wrap
import pyfits as pf
import math



# read input variables
try:
    opts, args = getopt.getopt(sys.argv[1:],"hp:t:w:v:a:u:r:o:j:s:l:k:z:",["psiZeroDeg=","thetaZeroDeg=","psiWidthDeg=","thetaWidthDeg=","alphaIntDeg=","user_rse=","runname=","dirout=","displayJ=","plotview=","intervallow=","intervalhigh=","fitsorplot="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print 'xxx'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print 'xxx'
        sys.exit()
    elif opt in ("-p", "--psiZeroDeg"):
        psiZeroDeg = float(arg)
    elif opt in ("-t", "--thetaZeroDeg"):
        thetaZeroDeg = float(arg)
    elif opt in ("-w", "--psiWidthDeg"):
        psiWidthDeg = float(arg)
    elif opt in ("-v", "--thetaWidthDeg"):
        thetaWidthDeg = float(arg)
    elif opt in ("-a", "--alphaIntDeg"):
        alphaIntDeg = float(arg)
    elif opt in ("-u", "--user_rse"):
        user_rse = float(arg)
    elif opt in ("-r", "--runname"):
        runname = arg
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg
    elif opt in ("-j", "--displayJ"):
        displayJ = arg
    elif opt in ("-s", "--plotview"):
        plotview = arg
    elif opt in ("-l", "--intervallow"):
        intervallow = float(arg)
    elif opt in ("-k", "--intervalhigh"):
        intervalhigh = float(arg)
    elif opt in ("-z", "--fitsorplot"):
        fitsorplot = arg
        
print ""


runnumber=["+6","+4","+2","+0","-2","-4","-6"]

inputfilename="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse5"+"_alphaint"+str(alphaIntDeg)+"deg.spectrum"
inputpath=outputdirectory+"/"+runname+"/gDM_MMIN_SUBS-1.e"+runnumber[0]+"/"+inputfilename

firstspectrum = np.loadtxt(inputpath)

clspectrum = np.ndarray((len(runnumber), len(firstspectrum)))

print clspectrum.shape

for i in range(len(runnumber)):
    print i
    inputfilename="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse5"+"_alphaint"+str(alphaIntDeg)+"deg.spectrum"
    inputpath=outputdirectory+"/"+runname+"/gDM_MMIN_SUBS-1.e"+runnumber[i]+"/"+inputfilename
    inputspectrum = np.loadtxt(inputpath)
    for j in range(len(inputspectrum)):
        clspectrum[i,j] = inputspectrum[j]

plot_powerspectrum = plt.figure(figsize=(12, 8)) 
plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()  
plt.xscale('log')
plt.yscale('log')
     
ell = np.arange(len(firstspectrum))
print len(ell)
# 
# 
for i in range(len(runnumber)):
    if i==0:
        COLOR = 'k'
    if i==1:
        COLOR = 'r'
    if i==2:
        COLOR = 'magenta'
    if i==3:
        COLOR = 'darkviolet'
    if i==4:
        COLOR = 'b'
    if i==5:
        COLOR = 'darkturquoise'
    if i==6:
        COLOR = 'orange'
    if i==7:
        COLOR = 'yellow'
    if i==8:
        COLOR = 'violet'
    if i==9:
        COLOR = 'darkgray'
    if i==10:
        COLOR = 'pink'
        
    p1 = plt.plot(ell, ell * (ell+1) * clspectrum[i,:]/(2*np.pi),color=COLOR,  linestyle='-', linewidth=1, label="10^"+str(runnumber[i]))   
    

#             
title = 'Angular power spectrum, for different minimal Clump masses in units of M_sun'
plt.title('\n'.join(wrap(title,80)))
plt.subplots_adjust(top=0.85)
plt.legend(loc='upper right')
#      
plt.show()

print "finished."
print ""

