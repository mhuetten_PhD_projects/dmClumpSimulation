#============= INSTRUCTIONS =================
# Purpose: import clump data from '....drawn' to 'list_example.txt'
# input: decay...........drawn
# output: list_example.txt

# Import all libraries needed 
import pandas as pd
import os
import sys
import getopt
import warnings

def main(argv):
	
	###########################################################################
	#  read input variables:
	
	infile = 'empty'

	try:
	    opts, args = getopt.getopt(sys.argv[1:],"i:o:",["infile=","outfile="])
	except getopt.GetoptError:
	    print('Wrong input. The input options are:')
	    print('-i or --infile for reading the input FITS file')
	    sys.exit(2)
	for opt, arg in opts:
	    if opt == '-h':
	        # help option
	        print('The input options are:')
	        print('-i or --infile for reading the input FITS file')
	        sys.exit()
	    elif opt in ("-i", "--infile"):
	        infile = str(arg)
	    elif opt in ("-o", "--outfile"):
			outfile = str(arg)
	
	if infile == 'empty':
	    print('  Please parse an input file')
	    sys.exit()

	print "This is transfer.py"

	#Locate the data
	data = infile
	
	#Assign the name of each column
	columnname_old=['clump', 'long', 'lat', 'mass', 'rvir', 'd', 'z', 'rhos', 'rs', 'alpha', 'beta', 'gamma', 'profile', 'J', 'JJcon']
	columnname_new=['clump', 'long', 'lat', 'mass', 'rvir', 'Mtid', 'Rtid', 'Mequdens', 'Requdens', 'Dgal', 'd', 'z', 'rhos', 'rs', 'alpha', 'beta', 'gamma', 'profile', 'J', 'JJcon']
	
	#Import the data
	try:
		df = pd.read_csv(data, header=None, comment='#', sep = r"\s*", names=columnname_old)
	except:
		df = pd.read_csv(data, header=None, comment='#', sep = r"\s*", names=columnname_new)
		#Delete the unwanted columns
		del df['Mtid']
		del df['Rtid']
		del df['Mequdens']
		del df['Requdens']
		del df['Dgal']
	df = df.ix[4:]
	
	#Delete the unwanted columns
	del df['mass']
	del df['J']
	del df['JJcon']
	
	#Add the 'type' column at the end 
	df['type'] = 'DSPH' #df.insert(1, 'type','DSPH') in the case of next to 'clump'
	
	#Rearrange the columns
	df = df[['clump','type','long','lat','d','z','rvir','rhos','rs','profile','alpha','beta','gamma']]
	
	#Edit the profile name
	df['profile'] = df['profile'].str.replace('EINASTO', 'kEINASTO')
	
	#Define the output formats
	fltfm = lambda x: '%7.1f' %x 
	scifm = lambda x: '%10.2E' %x
	strfm = lambda x: '%5s' %x
	intfm = lambda x: '%-2d' %x
	fm = {'type': strfm, 'long': fltfm, 'lat': fltfm, 'rhos': scifm, 'd': scifm, 'rs': scifm, 'rvir': scifm, 'z': fltfm, 'beta': fltfm, 'gamma': fltfm}
	
	#Write into the file
	with open(outfile, 'a') as f: 
		df.to_string(f, header=False, col_space=5, index=False, formatters=fm)
	f.close()
	
	print "transfer.py finished"
	
if __name__ == "__main__":
     
	main(sys.argv[1:])

##    end of file    ##########################################################
############################################################################### 
	
