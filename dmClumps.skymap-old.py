# -*- coding: utf-8 -*-

###############################################################################
# Changelog:
# 14/01/27: Corrected expression for healpix pixel resolution
#           now the resolution corresponds to the full diameter
#           of the pixel, which are assumed being circular.
#
# 14/02/06: changed central map transformation routine by saving the data first 
#           in a matrix and then writing to file (inspired by aliasing branch)
#           This should not increase much computation time.
#
# 14/02/07: minor changes, masked plot, evaluating power spectrum
#
# 14/02/27: corrected HEAVY BUG: division of clumpy output data now by clumpy
#           pixel size, not healpix pixel size!!
#
# 14/02/28: restricted plotting range in cartview to exact data size, additional
#           area made problems with all-sky plots.
#           First pixel in healpix map is not anymore blinded.
#
###############################################################################


# import needed modules:

import numpy as np
from numpy import loadtxt
import os
import sys
import getopt
import pyfits as pf
import math


# read input variables

try:
    opts, args = getopt.getopt(sys.argv[1:],"hp:t:w:v:a:u:r:o:j:s:l:k:z:m:",["psiZeroDeg=","thetaZeroDeg=","psiWidthDeg=","thetaWidthDeg=","alphaIntDeg=","user_rse=","runname=","dirout=","displayJ=","plotview=","intervallow=","intervalhigh=","fitsorplot=","minmass="])
except getopt.GetoptError:
    # This is raised when an unrecognized option is found in the argument list or when an option requiring an argument is given none.
    # The argument to the exception is a string indicating the cause of the error. The attributes msg and opt give the error message and related option 
    print 'The input options are:'
    print 'xxx'
    sys.exit(2)
for opt, arg in opts:
    if opt == '-h':
        # help option
        print 'The input options are:'
        print 'xxx'
        sys.exit()
    elif opt in ("-p", "--psiZeroDeg"):
        psiZeroDeg = float(arg)
    elif opt in ("-t", "--thetaZeroDeg"):
        thetaZeroDeg = float(arg)
    elif opt in ("-w", "--psiWidthDeg"):
        psiWidthDeg = float(arg)
    elif opt in ("-v", "--thetaWidthDeg"):
        thetaWidthDeg = float(arg)
    elif opt in ("-a", "--alphaIntDeg"):
        alphaIntDeg = float(arg)
    elif opt in ("-u", "--user_rse"):
        user_rse = float(arg)
    elif opt in ("-r", "--runname"):
        runname = arg
    elif opt in ("-o", "--dirout"):
        outputdirectory = arg
    elif opt in ("-j", "--displayJ"):
        displayJ = arg
    elif opt in ("-s", "--plotview"):
        plotview = arg
    elif opt in ("-l", "--intervallow"):
        intervallow = float(arg)
    elif opt in ("-k", "--intervalhigh"):
        intervalhigh = float(arg)
    elif opt in ("-z", "--fitsorplot"):
        fitsorplot = arg
    elif opt in ("-m", "--minmass"):
        minmass = arg


if fitsorplot!='fits':
    import pylab
import healpy as hp # warning: due to a bug in healpy, importing it before pylab can cause a segmentation fault in some circumstances.

# Branch between options "do all/do only calculation/do only plotting":
        
print ""
if fitsorplot!='plot':
    if fitsorplot=='fits':
        print " Load clumpy output data and write to healpix grid data file, make no plot"
    else: 
        print " Load clumpy output data, write to healpix grid data file and make plot"
        if plotview=='cart':
            print " plot will be cartesian."  
        if plotview=='moll':
            print " plot will be mollweide."
        if plotview!='cart' and plotview!='moll':
            print " Error: PLOTVIEW choice must be either cart or moll"    
    
    print " ***Note: Format of healpix grid data file is a simple list"
    
    # read clumpy output files as input files:
    if alphaIntDeg<0.1: 
        alphaIntDegb = alphaIntDeg
        alphaIntDeg = round(alphaIntDeg,2)
        inputfilename="annihil_gal2D_LOS"+str(int(math.floor(psiZeroDeg)))+","+str(int(thetaZeroDeg))+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.dat"
        inputpath=outputdirectory+"/"+inputfilename
        if os.path.isfile(inputpath)==0:
            inputfilename="annihil_gal2D_LOS"+str(int(math.ceil(psiZeroDeg)))+","+str(int(thetaZeroDeg))+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.dat"
        alphaIntDeg = alphaIntDegb
    else:
        inputfilename="annihil_gal2D_LOS"+str(int(psiZeroDeg))+","+str(int(thetaZeroDeg))+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"0deg.dat"
    
    outputfilename="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.healpix"
    outputfilename_fits = "annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.fits"
    outputfilename_spectrum="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.spectrum"
    outputfilename_spectrum_fits="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.spectrum.fits"
    inputfilename_spectrum="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.spectrum"
 
    inputpath=outputdirectory+"/"+inputfilename
    print " Loading file", inputpath
    print ""
    
    dataDeg = loadtxt(inputpath)
 
    data = np.zeros(shape=(dataDeg.shape))
    
    print " psi(LOS) in celestial coordinates: ",psiZeroDeg,"degrees"
    print " theta(LOS) in celestial coordinates: ",thetaZeroDeg,"degrees"
    
    
    # change theta and phi into standard spherical coordinates:
    if psiZeroDeg<0:
            psiZeroDeg = psiZeroDeg + 360.0 # go to psi:[0,2pi]
    thetaZeroDeg = -thetaZeroDeg + 90.0 # go to theta:[0,pi]
    
    print " Full width of FOV around LOS in psi direction: ",psiWidthDeg,"degrees"
    print " Full width of FOV around LOS in theta direction: ",thetaWidthDeg,"degrees"
    print " Delta theta,psi: ",alphaIntDeg,"degrees"
    
    data[:,1] = dataDeg[:,0]/180.0*np.pi # - np.pi # second row  now psi, flip coordinates
    
    datalength = len(data[:,1])
    print " this gives", datalength, "pixels in clumpy map"
    for i in range(datalength):
        if data[i,1]<0:
            data[i,1] = data[i,1] + 2*np.pi # go to psi:[0,2pi]
            
    data[:,0] = -dataDeg[:,1]/180.0*np.pi + np.pi/2 # first row  now theta, new coordinates theta:[0,pi] and flipped
    data[:,2] = dataDeg[:,2]
    data[:,3] = dataDeg[:,3]   
    data[:,4] = dataDeg[:,4]
    data[:,5] = dataDeg[:,5]
    data[:,6] = dataDeg[:,6]  
    
    # check out nxm dimensions:
    nPsi = 1
    for j in range(1,datalength):
        if data[j,1]!= data[j-1,1]:
            nPsi = nPsi+1
    print " ... thereof in psi direction:",nPsi,"pixels"
    nTheta = datalength/nPsi
    print " ... thereof in in theta direction:",nTheta,"pixels"
    print ""
    
    # extract psi, theta vector:
    psiVector = np.zeros(nPsi)
    thetaVector = np.zeros(nTheta)
    for j in range(nPsi):
        psiVector[j] = data[nTheta*j,1]
    for k in range(nTheta):    
        thetaVector[k] = data[k,0]
    
    # convert all coordinate parameters into radians:
    alphaInt = alphaIntDeg/180.0*np.pi
    psiWidth = psiWidthDeg/180.0*np.pi
    thetaWidth = thetaWidthDeg/180.0*np.pi
    psiZero =  psiZeroDeg/180.0*np.pi
    thetaZero = thetaZeroDeg/180.0*np.pi
    
    # determine dimension of healpix map, adapted to resolution of clumpy data: 
    n=0 
    resolution = np.pi
    while resolution>alphaInt:
        n = n+1   
        nside = 2**n
        resolution = hp.nside2resol(nside)*2.0/np.sqrt(np.pi) # compare with healpix documentation: this calculation gives the full diameter of a circular assumed region.
     
    nside = 2**(n-1)
    resolution = hp.nside2resol(nside)*2.0/np.sqrt(np.pi)
    resolutionDeg = resolution*180.0/np.pi
    
    print " NSIDE of haelpix map:",nside, "pixels"
    print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
    
    npix = hp.nside2npix(nside)
    print " size of healpix map:",npix, "pixels"
    
    pixelareaHealpix = 4*np.pi/npix
    pixelareaHealpixCheck = hp.nside2pixarea(nside)
    #pixelareaSteradian = 2*np.pi*(1-np.cos(hp.max_pixrad(nside)/2))
    print " exact pixel area in healpix map:", pixelareaHealpix, "sr (rad^2)"
    #print " check pixel area healpix map internal nside2pixarea function:", pixelareaHealpix, "rad^2"
    
   
    # what happens when psiZero-DeltaPsi>psiZero:
    # redefine psi coordinates:
    CoordOriginbool = 0
    if psiZero-(psiWidth/2)<0:
        print " FOV crosses psi=0: Redefine psi coordinate"
        CoordOriginbool = 1
        for i in range(nPsi):
            if psiVector[i]>np.pi:
                psiVector[i] = psiVector[i] - 2*np.pi # go to psi:[0,2pi]
    
    thetaMIN = min(thetaVector)
    thetaMAX = max(thetaVector)
    epsilonPsi = 2*alphaInt
    psiMIN = min(psiVector)-epsilonPsi
    print " psiMIN:", psiMIN/np.pi*180, "degrees"
    psiMAX =  max(psiVector)+epsilonPsi
    print " psiMAX:", psiMAX/np.pi*180, "degrees"
    
    # constrain ring ordered pix by theta:
    pixelnrMIN = hp.ang2pix(nside,thetaMIN,psiMIN)-2
    pixelnrMAX = hp.ang2pix(nside,thetaMAX,psiMAX)+2
    print " pixels of healpix map that are considered: ", pixelnrMIN,"< # <", pixelnrMAX

    # prepare writing to file:
    outputpath=outputdirectory+"/"+outputfilename
    print " write skymap to file:", outputpath
    
    f = open(outputpath, 'w')
    f.write("# number of pixels in healpix map: ")
    f.write(str(npix))
    f.write("\n")   
    f.write("#     healpix map pixel #    |   Jsmooth  |   <Jsub>   | Jcrossprod |    Jdrawn  |    Jtot")
    f.write("\n") 
    f.write("#                  |                         [Msol^2/kpc^5/sr]")
    f.write("\n")

    skymap = np.zeros(npix)-1.6375*10**(30)
    # prepare plotting:
    if fitsorplot!='fits':
        print " You have chosen to plot the skymap and that"
        if displayJ=='Jclumps':
            print " only clumpy component of DM halo is plotted."
            explanation="J_clump (only clumpy component of DM halo)"
        if displayJ=='Jtot': 
            print " both smooth and clumpy components of DM halo are plotted."
            explanation="J_tot (both smooth and clumpy components of DM halo)"
        if displayJ=='Jsmooth': 
            print " only smooth component of DM halo is plotted."
            explanation="J_smooth (only smooth component of DM halo)"
        if displayJ=='Jsum': 
            print "  Jsmooth +  <Jsub> + Jcrossprod are plotted."
            explanation="J_sum (Jsmooth +  <Jsub> + Jcrossprod)"  
        print "" 

###################### this is the central algorithm: #########################

    print " now transform rectangular grid to healpix spherical grid..."
    print ""
    
    for i in range(pixelnrMIN,pixelnrMAX):
        [theta, psi] = hp.pix2ang(nside,i)
        
        if CoordOriginbool == 1:
             # psi -> 360 - psi, psimin -> 360 -psi
            if psi>np.pi:
                psi = psi - 2*np.pi
                
        # constrain ring ordered pixels by phi:
        if psi>=psiMIN and psi<=psiMAX:
            # determine clumpy coordinates (thetaValue, psiValue), which are
            # closest to the pixel coordinate (theta, psi):
            minimumvectorPsi = abs(psiVector-psi)
            psiIndex = np.argmin(minimumvectorPsi)
            #psiValue = psiVector[psiIndex]
            minimumvectorTheta = abs(thetaVector-theta) 
            thetaIndex = np.argmin(minimumvectorTheta)
            #thetaValue = thetaVector[thetaIndex]
            
           
            pixelareaClumpy =  2 * np.pi * (1 - np.cos(alphaInt)) #np.pi * alphaInt * alphaInt#(np.cos(theta) - np.cos(theta + alphaInt))
           
            f.write(str(i))
            f.write(" ")
            f.write(str(data[nTheta*psiIndex+thetaIndex,2]/pixelareaClumpy))
            f.write(" ")
            f.write(str(data[nTheta*psiIndex+thetaIndex,3]/pixelareaClumpy))
            f.write(" ")
            f.write(str(data[nTheta*psiIndex+thetaIndex,4]/pixelareaClumpy))
            f.write(" ")
            f.write(str(data[nTheta*psiIndex+thetaIndex,5]/pixelareaClumpy))
            f.write(" ")
            f.write(str(data[nTheta*psiIndex+thetaIndex,6]/pixelareaClumpy))
            f.write("\n")
    
            # directly keep skymap data for saving power spectrum or plotting, 
            # for not needing to load the saved text file again.
            if displayJ=='Jclumps':
                skymap[i] = data[nTheta*psiIndex+thetaIndex,5]/pixelareaClumpy
            if displayJ=='Jtot': 
                skymap[i] = data[nTheta*psiIndex+thetaIndex,6]/pixelareaClumpy
            if displayJ=='Jsmooth': 
                skymap[i] = data[nTheta*psiIndex+thetaIndex,2]/pixelareaClumpy
            if displayJ=='Jsum': 
                skymap[i] = (data[nTheta*psiIndex+thetaIndex,2]+data[nTheta*psiIndex+thetaIndex,3]+data[nTheta*psiIndex+thetaIndex,4])/pixelareaClumpy
            
###############################################################################
       
    f.close() 
    
    # remove original clumpy output data:
    # os.remove(inputpath)
    
    #print " maximum value of clumpy data:",max(jvector)
    #print " maximum value in healpix skymap:",max(skymap)
    
    print " Now save  power spectrum..."
    #clOUT = 1
    #clOUT = hp.anafast(skymap)
    #outputpath=outputdirectory+"/"+outputfilename_spectrum
    #np.savetxt(outputpath, clOUT)
    outputpath_fits = outputdirectory+"/"+outputfilename_fits
    outputpath_spectrum_fits = outputdirectory+"/"+outputfilename_spectrum_fits
    hp.write_map(outputpath_fits, skymap)
    ## compute power spectrum:
    
    lmax = int(np.pi/hp.nside2resol(nside))
    print " Write Anafast parameter file anafast.par..."
    # Create Anafast Input parameter file:
    filename =  "anafast"
    outputpath_anafastpar = str(outputdirectory)+"/%s.par" % filename 
    outputfile_anafastpar = open(outputpath_anafastpar, 'wb')
    
    
    # Fill Anafast parameter file Anafast.par:
    outputfile_anafastpar.write("simul_type = 1\n\n")
    outputfile_anafastpar.write("nlmax = ")
    outputfile_anafastpar.write(str(lmax))
    outputfile_anafastpar.write("\n\n")
    outputfile_anafastpar.write("infile = ")
    outputfile_anafastpar.write(outputpath_fits)
    outputfile_anafastpar.write("\n\n") 
    outputfile_anafastpar.write("maskfile = ''\n\n")
    outputfile_anafastpar.write("theta_cut_deg = 0.00\n\n")
    outputfile_anafastpar.write("regression = 1\n\n") # removes monopole term
    outputfile_anafastpar.write("plmfile = ''\n\n")
    outputfile_anafastpar.write("outfile = ")
    outputfile_anafastpar.write(outputpath_spectrum_fits)
    outputfile_anafastpar.write("\n\n")
    outputfile_anafastpar.write("outfile_alms = ''\n\n") #outfile_alms=$ODIR/${SPARAMETER}_alm.fits 
    outputfile_anafastpar.write("won = 0\n\n")
    outputfile_anafastpar.write("iter_order = 0")
    outputfile_anafastpar.close()
    
    
# print skymap
if fitsorplot!='fits':
    from matplotlib import pyplot as plt
    from matplotlib.backends.backend_pdf import PdfPages as pdf
    from textwrap import wrap

    
    if fitsorplot=='plot':
        
  
        
        print " Generate only plot out of already existing healpix skymap data file"
        
        if displayJ=='Jclumps':
            print " only clumpy component of DM halo is plotted."
            explanation="J_clump (only clumpy component of DM halo)"
            Jcolumn = 4
        if displayJ=='Jtot': 
            print " both smooth and clumpy components of DM halo are plotted."
            explanation="J_tot (both smooth and clumpy components of DM halo)"
            Jcolumn = 5
        if displayJ=='Jsmooth': 
            print " only smooth component of DM halo is plotted."
            explanation="J_smooth (only smooth component of DM halo)"
            Jcolumn = 1
        if displayJ=='Jsum': 
            print "  Jsmooth +  <Jsub> + Jcrossprod are plotted."
            explanation="J_sum (Jsmooth +  <Jsub> + Jcrossprod)"     

        inputfilename="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.healpix"
        inputfilename_spectrum="annihil_gal2D_LOS"+str(psiZeroDeg)+","+str(thetaZeroDeg)+"_FOV"+str(int(psiWidthDeg))+"x"+str(int(thetaWidthDeg))+"_rse"+str(int(user_rse))+"_alphaint"+str(alphaIntDeg)+"deg.spectrum"

        inputpath=outputdirectory+"/"+inputfilename
        print " read skymap from data file:", inputpath
        
        skymapData = loadtxt(inputpath)
        
        alphaInt = alphaIntDeg/180.0*np.pi
        n=0 
        resolution = np.pi
        while resolution>alphaInt:
            n = n+1   
            nside = 2**n
            resolution = hp.nside2resol(nside)*2.0/np.sqrt(np.pi)
     
        nside = 2**(n-1)
        npix = hp.nside2npix(nside)
        pixelareaHealpix = 4*np.pi/npix
        print " NSIDE of healpix map:",nside, "pixels"
    
        skymap = np.zeros(npix)-1.6375*10**(30)
        
        resolution = hp.nside2resol(nside)*2.0/np.sqrt(np.pi)
        resolutionDeg = resolution*180.0/np.pi
        print " resolution of healpix map (full pixel diameter for approximately circular pixels):", round(resolutionDeg,3), "degrees"
        
        #print len(skymapData[:,0])
        
        for i in range(len(skymapData[:,0])):
            if displayJ=='Jsum': 
                skymap[skymapData[i,0]] = skymapData[i,1]+skymapData[i,2]+skymapData[i,3]
            else:
                skymap[skymapData[i,0]] = skymapData[i,Jcolumn]
            
        if psiZeroDeg<0:
            psiZeroDeg = psiZeroDeg + 360.0 # go to psi:[0,2pi]
        thetaZeroDeg = -thetaZeroDeg + 90.0 # go to theta:[0,pi]
    
        
    else:
        print " Now plot skymap:"

    psiZeroLon = psiZeroDeg
    thetaZeroLat = 90-thetaZeroDeg
    
    #print " maximum value in skymap:", round(max(skymap),3), "M_sun^2/kpc^5/sr"
    #print " J-factor in that pixel:", round(max(skymap)*pixelareaHealpix,3), "M_sun^2/kpc^5"
    
    XSIZE = int(360.0/resolutionDeg)/2
    if XSIZE > 3200:
        XSIZE = 3200
    if XSIZE < 2400:
        XSIZE = 2400
    XSIZE = 3200
    
    if plotview=='cart':
            plot_skymap = plt.figure(figsize=(9,11*thetaWidthDeg/psiWidthDeg))
            TITLE = 'J-factor skymap of %s\n for user_rse = %s around galactic position (Psi,theta)=(%s,%s) degrees,\n resolution = %s degs, minimal clump mass = %s M_sun'%(explanation,user_rse,psiZeroLon,thetaZeroLat,round(resolutionDeg,3),minmass)
            hp.cartview(skymap, fig=1,rot=[psiZeroLon,thetaZeroLat], flip='geo',xsize=10000, lonra= [-psiWidthDeg*0.5,psiWidthDeg*0.5], latra= [-thetaWidthDeg*0.5,+thetaWidthDeg*0.5], norm='log',min=10**(intervallow), max=10**(intervalhigh), title=TITLE, unit='[M_sun^2/kpc^5/sr]')
            print " pixel area in healpix map (assume approximately circular pixels):", pixelareaHealpix, "sr (rad^2)"
    if plotview=='moll':
            plot_skymap = plt.figure(figsize=(11,8))
            plt.axis('off')
            title = 'J-factor map of %s for user_rse = %s \n around galactic position (Psi,theta)=(%s,%s) degrees, resolution = %s degs, minimal clump mass = %s M_sun'%(explanation,user_rse,psiZeroLon,thetaZeroLat,round(resolutionDeg,3),minmass)
            plt.title('\n'.join(wrap(title,90)))

            hp.mollview(skymap, fig=1, norm='log',min=10**(intervallow), max=10**(intervalhigh),xsize=XSIZE,rot=[0,thetaZeroLat], flip='geo', title='', unit='[M_sun^2/kpc^5/sr]')

            print " pixel area in healpix map (assume approximately circular pixels):", pixelareaHealpix, "sr (rad^2)"

    grat = min(psiWidthDeg,thetaWidthDeg)/5

    hp.graticule(dpar=grat, dmer=grat/2)

    # plot  power spectrum:
    
    inputpath=outputdirectory+"/"+inputfilename_spectrum
       
    if os.path.isfile(inputpath)==True:
        print ""
        print " Now  plot power spectrum:" 
        print " read power spectrum from data file:", inputpath
             
        clOUT = np.loadtxt(inputpath)
             
        plot_powerspectrum = plt.figure(figsize=(9, 6))
        ell = np.arange(len(clOUT))
              
        plt.plot(ell, ell * (ell+1) * clOUT/(2*np.pi),color='r')#
        plt.xlabel('multipole index l'); plt.ylabel('l(l+1)c_l/2pi'); plt.grid()
        plt.xscale('log')
        plt.yscale('log')
        pylab.xlim([1,len(clOUT)])
        #pylab.ylim([10**(-4),10**6])
        title = 'Angular power spectrum  of skymap for user_rse = %s around galactic position\n (Psi,theta)=(%s,%s) degrees, resolution = %s degs, DeltaAngle = %s degs,\n minimal clump mass = %s M_sun'%(user_rse,psiZeroLon,thetaZeroLat,round(resolutionDeg,3),psiWidthDeg,minmass)
        plt.title('\n'.join(wrap(title,85)))
        plt.subplots_adjust(top=0.85)
    
    
    plt.show()
    
print " finished."
print ""

